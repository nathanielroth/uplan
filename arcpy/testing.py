'''
Created on Mar 5, 2015

@author: roth
'''
import os
import arcpy
import pandas as pd
import numpy as np
from Utilities import MergeDataFrames

arcpy.env.workspace=r"M:\UPlan4\repository\testing\calaveras.gdb"
arcpy.env.overwriteOutput = Trues

demand = 100
   
# Get gp table
gpTableName = "up_bg_gp_avail_ts1"
gparray = arcpy.da.TableToNumPyArray("up_bg_gp_avail_ts1", ['pclid','gp_ind'])
gpdf = pd.DataFrame(gparray)

# Get dev space table
dsTableName = "up_cons_ts1"
dsarray = arcpy.da.TableToNumPyArray("up_cons_ts1", ['pclid','SUM_dev_ac_ind'])
dsdf = pd.DataFrame(dsarray)

# get weight table
wtTableName = "up_weights"
wtFieldName = 'wt_ts1_ind'
wtarray = arcpy.da.TableToNumPyArray("up_weights",['pclid','wt_ts1_ind']) 
wtdf = pd.DataFrame(wtarray)

# get prior alloc table


allocdf = MergeDataFrames([gpdf,dsdf,wtdf],'pclid')

# Do allocation
allocdf['cumarea'] = allocdf[(allocdf['gp_ind'] == 1) & (allocdf['SUM_dev_ac_ind'] > 0)].sort(columns = 'wt_ts1_ind',ascending=False)['SUM_dev_ac_ind'].cumsum()
allocdf['alloc_pct'] = 0.
allocdf['alloc_pct'][(allocdf['cumarea'] < demand)] = 1.

# Exporting to numpy array and table
exdf = allocdf.loc[:,['pclid','alloc_pct']]
arr = np.array(exdf.to_records(False))
arr.dtype = [('pclid','int'),('alloc_pct','float')]
arcpy.da.NumPyArrayToTable(arr,os.path.join(r"M:\UPlan4\repository\testing\calaveras.gdb","up_test"))
pass