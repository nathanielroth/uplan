import arcpy
import LayerManagement, UPConfig
import os

class Toolbox(object):
    def __init__(self):
        """Define the toolbox (the name of the toolbox is the name of the
        .pyt file)."""
        self.label = "Import Layers"
        self.alias = "import_layer"

        # List of tool classes associated with this toolbox
        self.tools = [CreateUPlanGDB, ImportBaseGeometry, ImportConstraint, ImportAttractor, ImportGeneralPlan, ImportSALayer]

class CreateUPlanGDB(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Create UPlan Geodatabase"
        self.description = "This tool creates a geodatabase for all UPlan layers and tables"
        self.canRunInBackground = False

    def getParameterInfo(self):
        """Define parameter definitions"""
        # First parameter
        UPlan_gdb_folder = arcpy.Parameter(
            displayName="Location for UPlan GDB",
            name="uplan_gdb_folder",
            datatype="DEFolder",
            parameterType="Required",
            direction="Input")
        
         # Second parameter
        UPlan_gdb_name = arcpy.Parameter(
            displayName="Name for UPlan GDB",
            name="uplan_gdb_name",
            datatype="GPString",
            parameterType="Required",
            direction="Input")
          
        
        params = [UPlan_gdb_folder, UPlan_gdb_name]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        Uplan_gdb_folder = parameters[0].valueAsText
        Uplan_gdb_name = parameters[1].valueAsText
       
        #create UPlan geodatabase
        arcpy.CreateFileGDB_management(Uplan_gdb_folder, Uplan_gdb_name)
        
        #create UPC_*tables
        UPConfig.EmptyOrCreateUPCTables(os.path.join(Uplan_gdb_folder, Uplan_gdb_name) + '.gdb')                            
        return


class ImportBaseGeometry(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Import Base Geometry Layer"
        self.description = ""
        self.canRunInBackground = False

    def getParameterInfo(self):
        """Define parameter definitions"""
        # First parameter
        in_base_geom_layer = arcpy.Parameter(
            displayName="Base Geometry Layer",
            name="geom_layer",
            datatype="GPFeatureLayer",
            parameterType="Required",
            direction="Input")
                               
        #Second parameter
        Descriptive_name=arcpy.Parameter(
            displayName="Base Geometry Descriptive Name",
            name="desc_name",
            datatype="GPString",
            parameterType="Required",
            direction="Input") 
        
        #Third parameter
        UPlan_geodatabase=arcpy.Parameter(
            displayName="UPlan File Geodatabase",
            name="up_gdb",
            datatype="DEWorkspace",
            parameterType="Required",
            direction="Input")  
        
        
        params = [in_base_geom_layer, Descriptive_name, UPlan_geodatabase]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        in_layer = parameters[0].valueAsText
        desc_name = parameters[1].valueAsText
        Uplan_gdb = parameters[2].valueAsText
        
        
        LayerManagement.ImportBaseGeom(Uplan_gdb, in_layer, desc_name)
                       
    
        return

class ImportConstraint(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Import Constraint Layer"
        self.description = "This tool imports a constraint layer to the UPlan geodatabase"
        self.canRunInBackground = False

    def getParameterInfo(self):
        """Define parameter definitions"""
        # First parameter
        InConstraint = arcpy.Parameter(
            displayName="Constraint Layer",
            name="constraint_layer",
            datatype="GPFeatureLayer",
            parameterType="Required",
            direction="Input")
        
        #Second parameter
        LongName=arcpy.Parameter(
            displayName="Constraint Layer Long Name",
            name="desc_name",
            datatype="GPString",
            parameterType="Required",
            direction="Input") 
                                
         #Third parameter
        UPlan_geodatabase=arcpy.Parameter(
            displayName="UPlan File Geodatabase",
            name="up_gdb",
            datatype="DEWorkspace",
            parameterType="Required",
            direction="Input")      
                                  

        params = [InConstraint, LongName, UPlan_geodatabase]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        in_layer = parameters[0].valueAsText
        Uplan_gdb = parameters[2].valueAsText
        desc_name = parameters[1].valueAsText
        
        LayerManagement.ImportConstraintLayer(in_layer, Uplan_gdb, desc_name)
                       
    
        return
    
class ImportAttractor(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Import Attractor Layer"
        self.description = "This tool imports an attractor layer to the UPlan geodatabase"
        self.canRunInBackground = False

    def getParameterInfo(self):
        """Define parameter definitions"""
        # First parameter
        InAttractor = arcpy.Parameter(
            displayName="Attractor Layer",
            name="attractor_layer",
            datatype="GPFeatureLayer",
            parameterType="Required",
            direction="Input")
        
        #Second parameter
        LongName=arcpy.Parameter(
            displayName="Attractor Layer Long Name",
            name="desc_name",
            datatype="GPString",
            parameterType="Required",
            direction="Input") 
                                
         #Third parameter
        UPlan_geodatabase=arcpy.Parameter(
            displayName="UPlan File Geodatabase",
            name="up_gdb",
            datatype="DEWorkspace",
            parameterType="Required",
            direction="Input")      
                                  

        params = [InAttractor, LongName, UPlan_geodatabase]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        in_layer = parameters[0].valueAsText
        Uplan_gdb = parameters[2].valueAsText
        desc_name = parameters[1].valueAsText
        
        LayerManagement.ImportAttractorLayer(in_layer, Uplan_gdb, desc_name)
                       
    
        return
    
class ImportGeneralPlan(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Import General Plan Layer"
        self.description = "This tool imports a general plan layer to the UPlan geodatabase"
        self.canRunInBackground = False

    def getParameterInfo(self):
        """Define parameter definitions"""
        # First parameter
        InGP = arcpy.Parameter(
            displayName="General Plan Layer",
            name="gp_layer",
            datatype="GPFeatureLayer",
            parameterType="Required",
            direction="Input")
        
        #Second parameter
        LongName=arcpy.Parameter(
            displayName="General Plan Layer Descriptive Name",
            name="desc_name",
            datatype="GPString",
            parameterType="Required",
            direction="Input") 
        
        #Third parameter
        GPCatField=arcpy.Parameter(
            displayName="Field within the layer that contains the general plan codes",
            name="gp_cat_field",
            datatype="Field",
            parameterType="Required",
            direction="Input")  
        GPCatField.parameterDependencies = [InGP.name]
                                
         #Fourth parameter
        UPlan_geodatabase=arcpy.Parameter(
            displayName="UPlan File Geodatabase",
            name="up_gdb",
            datatype="DEWorkspace",
            parameterType="Required",
            direction="Input")      
                                  

        params = [InGP, LongName, GPCatField, UPlan_geodatabase]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        in_layer = parameters[0].valueAsText
        Uplan_gdb = parameters[3].valueAsText
        desc_name = parameters[1].valueAsText
        gp_field = parameters[2].valueAsText
          
        LayerManagement.ImportGPLayer(in_layer, Uplan_gdb, gp_field, desc_name)
                       
    
        return
    
class ImportSALayer(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Import SubArea Layer"
        self.description = "Imports an SubArea layer to the geodatabase"
        self.canRunInBackground = False

    def getParameterInfo(self):
        """Define parameter definitions"""
        # First parameter
        InSALayer = arcpy.Parameter(
            displayName="SubArea Layer",
            name="subarea_layer",
            datatype="GPFeatureLayer",
            parameterType="Required",
            direction="Input")
        
        #Second parameter
        LongName=arcpy.Parameter(
            displayName="SubArea Layer Descriptive Name",
            name="desc_name",
            datatype="GPString",
            parameterType="Required",
            direction="Input") 
        
        #Third parameter
        SAIDField=arcpy.Parameter(
            displayName="Field within the layer that contains the SubArea codes (or IDs)",
            name="sa_id_field",
            datatype="Field",
            parameterType="Required",
            direction="Input")  
        SAIDField.parameterDependencies = [InSALayer.name]
                
        #Fourth parameter
        SANameField=arcpy.Parameter(
            displayName="Field within the layer that contains the SubArea descriptions (or Names)",
            name="sa_name_field",
            datatype="Field",
            parameterType="Required",
            direction="Input")  
        SANameField.parameterDependencies = [InSALayer.name]
                                
         #Fifth parameter
        UPlan_geodatabase=arcpy.Parameter(
            displayName="UPlan File Geodatabase",
            name="up_gdb",
            datatype="DEWorkspace",
            parameterType="Required",
            direction="Input") 

        #Sixth parameter        
        SearchLength=arcpy.Parameter(
            displayName="Maximum distance between centroids and edge of SubArea",
            name="search_length",
            datatype="GPLinearUnit",
            parameterType="Required",
            direction="Input")      
                                  

        params = [InSALayer, UPlan_geodatabase, LongName, SAIDField, SANameField, SearchLength]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        InSALayer = parameters[0].valueAsText
        UPlanGDB = parameters[1].valueAsText
        LongName = parameters[2].valueAsText
        SAIDField = parameters[3].valueAsText
        SANameField = parameters[4].valueAsText
        SearchLength = parameters[5].valueAsText
        
        messages.addMessage("SA Code: {sanf}".format(sanf = SAIDField))
        messages.addMessage("SA Long Name: {sanf}".format(sanf = SANameField))
        
        messages = LayerManagement.ImportSALayer(InSALayer, UPlanGDB, LongName, SAIDField, SANameField, SearchLength, messages)
        
       
                       
    
        return