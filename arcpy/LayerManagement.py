'''
Created on Mar 20, 2015

@author: bjorkman, boynton
'''
import os
import datetime
import numpy as np
import numpy.lib.recfunctions as rfn
import arcpy
import pandas as pd
from Utilities import Logger
from arcpy import env

def CalculateFieldTo1(InWorkspace, LayerName):
    '''
    Creates a new integer field with the same name as the layer and calculates all rows equal to 1
    
    Called by:
    ImportConstraintLayer
    ImportAttractorLayer
    
    Arguments:
    InWorkspace: The GDB that contains the layer
    LayerName: The name of the layer
    
    Returns: None
    '''    
    #Add a new field, integer type, with name = to layer name and calculate the field =1
    InLayer = os.path.join(InWorkspace, LayerName)
             
    #Add new field to constraint layer
    arcpy.AddField_management(InLayer, LayerName, "SHORT")
    
    #Calculate field layer_num = 1
    arcpy.CalculateField_management(InLayer, LayerName, 1, "PYTHON_9.3")

def AddToUPC_Layers(InWorkspace, LayerName, long_name, LayerRole):
    '''
    Adds a row to the upc_layers table to log when the layer was added to the GDB and what Role it is assigned to
    
    Called by:
    ImportBaseGeom
    ImportConstraintLayer
    ImportAttractorLayer
    ImportGPLayer
    ImportSALayer
    
    Arguments:
    InWorkspace: The GDB that contains the layer
    LayerName: Name of the layer that was added to the GDB
    long_name: The descriptive name of the layer
    LayerRole: The Role the layer was assigned to
    
    Returns: None
    '''  
    #Add a row to the existing upc_layers table with the name of the constraint layer, the long name, the date added and the role (constraint)  
    InTable = os.path.join(InWorkspace, 'upc_layers')
      
    #Add layer to upc_layers table
    fields = ['FCName', 'LongName', 'DateAdded', 'Role']
    cur = arcpy.da.InsertCursor(InTable, fields)
    
    Date = str(datetime.datetime.now().date())
    cur.insertRow((LayerName, long_name, Date, LayerRole))
    del cur

def ImportBaseGeom(UPlanGDB,InSHP,DescName):
    '''
    *Imports a Base Geometry layer to the GDB 
    *Adds a field called up_polyid and calculates it equal to the ObjectID
    *Creates a Centroid layer from the Base Geometry layer and names it BaseGeom_cent
    *Adds a record to the upc_layers table for this Base Geometry layer and the centroid layer
    
    Calls: 
    AddToUPC_Layers
    
    Called by:
    The Import Base Geometry Toolbox
    
    Arguments:
    UPlanGDB: The UPlan GDB (where the layer will be imported)
    InSHP: The shapefile to import
    DescName: The descriptive name of the layer
    
    Returns: None
    '''  
    arcpy.env.workspace = UPlanGDB 
    
    #Register it with the layer tracker
    if os.path.basename(InSHP)[-4:]==".shp":
        SHPName = os.path.basename(InSHP)[:-4]
    else: 
        SHPName = os.path.basename(InSHP)
    AddToUPC_Layers(UPlanGDB,SHPName,DescName, 'BaseGeom_bnd')
    
    #Import the shp to the GDB
    arcpy.FeatureClassToFeatureClass_conversion(InSHP,UPlanGDB,SHPName)
    
    #Add int field called "up_polyid" and calc = to object ID
    arcpy.AddField_management(SHPName,"up_polyid","SHORT")
    arcpy.CalculateField_management(SHPName,"up_polyid","!OBJECTID!","PYTHON_9.3")
    
    #Create centroid layer and drop all fields except for "up_polyid" (and shape and objectid).
    arcpy.env.overwriteOutput = True
    arcpy.FeatureToPoint_management(SHPName,"poly_cent","CENTROID")
    
    Fields = arcpy.ListFields(SHPName)
    FieldList = []
    for Field in Fields:
        if not Field.required:
            if not Field.name == "up_polyid":
                FieldList.append(Field.name)
    arcpy.DeleteField_management("poly_cent",FieldList)   
    
    #add centroid layer to layer tracker table 
    AddToUPC_Layers(UPlanGDB,'poly_cent','Base Geometry Centroids (Created from: ' + SHPName + ')','BaseGeom_cent')
    
def ImportConstraintLayer(InConstraint, UPlanGDB, LongName):
    '''
    *Imports a Constraint layer to the GDB
    *Creates a field with the same name as the layer and calculates it equal to 1
    *Adds a record to the upc_layers table for this constraint layer
    
    Calls: 
    CalculateFieldTo1
    AddToUPC_Layers
    
    Called by:
    Import Constraint Toolbox
    
    Arguments:
    InConstraint: The layer to be added to the GDB as a constraint
    UPlanGDB: The UPlan GDB (where the layer will be imported)
    LongName: The descriptive name of the layer
    
    Returns: None
    '''  
    #Import a feature class layer to the existing geodatabase as a constraint
  
    # Set workspace
    env.workspace = UPlanGDB
    if os.path.basename(InConstraint)[-4:]==".shp":
        SHPName = os.path.basename(InConstraint)[:-4]
    else: 
        SHPName = os.path.basename(InConstraint)
        
    #Add layer to geodatabase
    arcpy.FeatureClassToGeodatabase_conversion(InConstraint, UPlanGDB)
    
    #calculate a new field = 1
    CalculateFieldTo1(UPlanGDB, SHPName)
    
    #add constraint layer to layer tracker table 
    AddToUPC_Layers(UPlanGDB, SHPName, LongName, 'Constraint')
    
def ImportAttractorLayer(InAttractor, UPlanGDB, LongName):
    '''
    *Imports an Attractor layer to the GDB
    *Creates a field with the same name as the layer and calculates it equal to 1
    *Adds a record to the upc_layers table for this attractor layer
    
    Calls: 
    CalculateFieldTo1
    AddToUPC_Layers
    
    Called by:
    Import Attractor Toolbox
    
    Arguments:
    InAttractor: The layer to be added to the GDB as an attractor
    UPlanGDB: The UPlan GDB (where the layer will be imported)
    LongName: The descriptive name of the layer
    
    Returns: None
    '''  
    #Import a layer to use as an attractor and record it to the Layer Tracker Table
    
    # Set workspace
    env.workspace = UPlanGDB
    if os.path.basename(InAttractor)[-4:]==".shp":
        SHPName = os.path.basename(InAttractor)[:-4]
    else: 
        SHPName = os.path.basename(InAttractor)
    #Add layer to geodatabase
    arcpy.FeatureClassToGeodatabase_conversion(InAttractor, UPlanGDB)
    
    #calculate a new field = 1
    CalculateFieldTo1(UPlanGDB, SHPName)
    
    #add constraint layer to layer tracker table 
    AddToUPC_Layers(UPlanGDB, SHPName, LongName, 'Attractor')

def ImportGPLayer(InGPLayer, UPlanGDB, GPCatField, LongName):
    '''
    *Imports an General Plan layer to the GDB
    *Adds a record to the upc_layers table for this attractor layer
    *Updates the upc_timesteps table with the new general plan information
    
    Calls: 
    AddToUPC_Layers
    
    Called by:
    Import General Plan Toolbox
    
    Arguments:
    InGPLayer: The layer to be added to the GDB as a general plan
    UPlanGDB: The UPlan GDB (where the layer will be imported)
    GPCatField: Field within the layer that contains the general plan codes
    LongName: The descriptive name of the layer
    
    currently disabled...
    Timesteps: What timestep(s) to assign this general plan layer to
    
    Returns: None
    '''  
    #Import a general plan layer record it's role, and record the gp class field name.
    arcpy.env.workspace = UPlanGDB
    if os.path.basename(InGPLayer)[-4:]==".shp":
        SHPName = os.path.basename(InGPLayer)[:-4]
    else: 
        SHPName = os.path.basename(InGPLayer)
        
    #Add layer to geodatabase
    arcpy.FeatureClassToGeodatabase_conversion(InGPLayer, UPlanGDB)
    
    #add general plan layer to layer tracker table 
    AddToUPC_Layers(UPlanGDB, SHPName, LongName, 'GeneralPlan')
    
#     #update the upc_timesteps table
#     for Timestep in Timesteps:
#         cur = arcpy.UpdateCursor('upc_timesteps',where_clause = r"Code = '" + Timestep + r"'")
#         row = cur.next()
#         row.setValue('GPLayer',SHPName)
#         row.setValue('GPField',GPCatField)
#         cur.updateRow(row)

def ImportSALayer(InSALayer, UPlanGDB, LongName, SAIDField, SANameField, SearchLength, messages):
    '''
    *Imports an SubArea layer to the GDB
    *Adds a record to the upc_layers table for this SubArea layer
    *Updates the upc_subareas table with the new SubAreas
    *Updates the 3 SubArea KeyNames in the upc_key table
    
    Calls: 
    AddToUPC_Layers
    
    Called by:
    Import General Plan Toolbox
    
    Arguments:
    InSALayer: The layer to be added to the GDB as SubAreas
    UPlanGDB: The UPlan GDB (where the layer will be imported)
    LongName: The descriptive name of the layer
    SAIDField: Field within the layer that contains the SubArea codes (or IDs)
    SANameField: Field within the layer that contains the SubArea descriptions (or Names)
    SearchLength: When assigning Base Geometry centroids to SubAreas, this is the maximum distance a centroid can be outside of a SubArea polygon and still be assigned to it 
    
    Returns: None
    '''  
    #Create a function to import a Subarea Layer, record the subarea_id and replace any prior subarea records
    arcpy.env.workspace = UPlanGDB
    if os.path.basename(InSALayer)[-4:]==".shp":
        SHPName = os.path.basename(InSALayer)[:-4]
    else: 
        SHPName = os.path.basename(InSALayer)
        
    #Add layer to geodatabase
    arcpy.FeatureClassToGeodatabase_conversion(InSALayer, UPlanGDB)
    
    #add general plan layer to layer tracker table 
    AddToUPC_Layers(UPlanGDB, SHPName, LongName, 'Subareas')
    
    #clear sub areas table
    arcpy.DeleteRows_management('upc_subareas')
    
    #populate sub areas table with attributes from the new layer
    Subareas= []
    rows = arcpy.SearchCursor(SHPName)
    for row in rows:
        SA = {}
        SA['sa']= row.getValue(SAIDField)
        SA['SAName'] = row.getValue(SANameField)
        if SA not in Subareas: # don't add a SA that is already in the list
            Subareas.append(SA)   
    print(Subareas)
    
   
    fields = ['Code','Name']
    cur = arcpy.da.InsertCursor('upc_subareas',fields)
    for x in range(0,len(Subareas)):
        sa = Subareas[x]['sa']
        SAName = Subareas[x]['SAName']
        cur.insertRow((sa,SAName))
    del cur
    
    #update the upc_key table with the new subarea
    cur = arcpy.UpdateCursor('upc_key',where_clause = r"KeyName = 'Subarea_bnd'")
    row = cur.next()
    row.setValue('KeyValue',SHPName)
    cur.updateRow(row)
    del cur
    
    cur = arcpy.UpdateCursor('upc_key',where_clause = r"KeyName = 'Subarea_id'")
    row = cur.next()
    row.setValue('KeyValue',SAIDField)
    cur.updateRow(row)
    del cur
    
    cur = arcpy.UpdateCursor('upc_key',where_clause = r"KeyName = 'Subarea_search'")
    row = cur.next()
    row.setValue('KeyValue',str(SearchLength))
    cur.updateRow(row)
    del cur
    return(messages)
    
if __name__ == "__main__":
    Logger("Layer management")
    dbpath = r"..\testing" 
    dbname = 'calaveras.gdb'
      
#     ###Add Base Gemoetry Tool
#     InGDB = r"D:\Projects\UPlan\UPlan4\testing\calaveras.gdb"
#     InSHP = r"D:\Projects\AmadorUPlan\UPlan2_6\data\Amador_working.gdb\Parcels_20130405" 
#     LongName = 'Amador Parcels'   
#        
#     Logger("Importing Base Geometry")
#     ImportBaseGeom(InGDB,InSHP, LongName)

#     ###Add Constraint Tool  
#     GDB = os.path.join(dbpath,dbname)
#     LongName='tricolored blackbird layer'
#     #InConstraint = r'D:\Projects\RAMP2014\GIS\2015\TCB_habitat.shp'
#     InConstraint = r'G:\Public\RAMP\PilotProject2014\SpatialData_Feb15Update\TCB\tcb_4mi_diss.shp'
#      
#     Logger("Importing Constraint Layer")  
#     ImportConstraintLayer(InConstraint, GDB, LongName)
#     
#     ###Add Attractor Tool
#     GDB = os.path.join(dbpath,dbname)
#     LongName='Pacific Ocean'
#     InAttractor = r'G:\Public\StatewideData\Ocean\ocean.shp'
#       
#     Logger("Importing Attraction Layer")  
#     ImportAttractorLayer(InAttractor, GDB, LongName)
#     
#     ###Add General Plan Tool
#     GDB = os.path.join(dbpath,dbname)
#     LongName=r"Riverbank's General Plan"
#     InGP = r'G:\Public\Greenprint\SJV\Collection\GIS\LandusePlanning\FromCity\stan\Riverbank_GeneralPlan_CANAD83.shp'
#     GPCatField = "LEGEND"
#     #This timestep needs to exist already:
#     Timesteps = ['ts1']
#      
#     Logger("Importing General Plan Layer")  
#     ImportGPLayer(InGP, GDB, GPCatField, LongName,Timesteps)
# 
#     ###Add SubArea Tool
#     GDB = os.path.join(dbpath,dbname)
#     SAshp = r'C:\Projects\UPlan4\testing\subarea2.shp'
#     SHPname = 'New Cities May 2015'
#     SACodeField = 'sa'
#     SANameField = 'saname'
#     SALength = 10
#       
#     Logger("Importing SubArea Layer")
#     ImportSALayer(SAshp,GDB,SHPname,SACodeField,SANameField,SALength)

    Logger("Management Complete") 
    
    print('Script Finished!')