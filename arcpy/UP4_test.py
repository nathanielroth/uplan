'''
old file - can probably be deleted (RB,NR 8/15/15)

Created on Jan 7, 2015

@author: roth
'''

import arcpy, datetime, os
#import numpy
#import pandas

def Logger(msg):
    print("| ".join([str(datetime.datetime.now()), msg]))

Logger("Beginning UPlan")

Logger("Loading Settings")
# config
fds = r"cal_fds"
dbpath = r"M:\UPlan4\Testing\cal_test.gdb"


LUInfo = {}
LUPriorities= ['ind','ret','ser','rh','rm','rl','rvl']
LUDemand = {}
d_ind = 72
d_ret = 105
d_ser = 219
d_rh  = 42
d_rm  = 7500
d_rl  = 8000
d_rvl = 50000

LUDemand['ind'] = d_ind
LUDemand['ret'] = d_ret
LUDemand['ser'] = d_ser
LUDemand['rh'] = d_rh
LUDemand['rm'] = d_rm
LUDemand['rl'] = d_rl
LUDemand['rvl'] = d_rvl

# General Plan Settings
gplu    = {}
gp_ind  = ['I','PD-I']
gp_ret  = ['Commercial','Commercial - Recreation',r'PS/SC','SC','CC','CC/H','PD',r'R1/SC','C-PD','CCH','CCL','CCR','CO','CR']
gp_ser  = ['Commercial','Commercial - Recreation',r'PS/SC','SC','CC','CC/H','PD',r'R1/SC','C-PD','CCH','CCL','CCR','CO','CR','PI']
gp_rh   = ['Residential - Medium - 6,000 sq ft','Residential - Low - 14,500 sq ft','R3','R3-PD','CC',r'CC/H','PD','CCR','RHD']
gp_rm   = ['Residential - Low - 14,500 sq ft','Residential - Low - 21,800','Residential - Low - 21,800 sq ft','Residential - Low - 1 acre',
          'Residential - Rural - 2 acres','Residential - Rural - 5 acres','Residential - Agricultural',
          'R3','R3-PD','R1','R1-PD','R2',r'RA/R1',r'R1/SC','C-PD','CCH','CCL','CCR','RHD','RMD','RLD','RH']
gp_rl   = ['Residential - Low - 1 acre','Residential - Rural - 2 acres','Residential - Rural - 5 acres','Residential - Agricultural','PD',r'RA/R1','CCH','CCR','RH','RR']
gp_rvl  = ['Mineral Resource','Agricultural Lands','Biological Resource',r'RA/R1','RA','WL','RP','RM','RTA','RTB']

gplu['ind'] = gp_ind
gplu['ret'] = gp_ret
gplu['ser'] = gp_ser
gplu['rh']  = gp_rh
gplu['rm']  = gp_rm
gplu['rl']  = gp_rl
gplu['rvl'] = gp_rvl

mixeduse = {}
mixeduse['CC'] = [['ret','rh'],['ret','ser'],['ser','rh'],['ret','ser','rh']]
mixeduse[r'CC/H'] = [['ret','rh'],['ret','ser'],['ser','rh'],['ret','ser','rh']]
mixeduse['PD'] = [['ret','rh'],['ret','ser'],['ser','rh'],['ret','ser','rh']]
mixeduse['CCR'] = [['ret','rh'],['ret','ser'],['ser','rh'],['ret','ser','rh']]

# Constraints
con_list = ['undevelopable','low_gw','vpools']
con_ind = {"undevelopable":1,"low_gw":1,"vpools":0.5}
con_ret = {"undevelopable":1,"low_gw":1,"vpools":0.5}
con_ser = {"undevelopable":1,"low_gw":1,"vpools":0.5}
con_rh  = {"undevelopable":1,"low_gw":0,"vpools":0.5}
con_rm  = {"undevelopable":1,"low_gw":0.25,"vpools":0.5}
con_rl  = {"undevelopable":1,"low_gw":0.25,"vpools":0.5}
con_rvl = {"undevelopable":1,"low_gw":0.25,"vpools":0.1}

# Build Constraints Dictionary
constraints = {}
constraints['ind'] = con_ind
constraints['ret'] = con_ret
constraints['ser'] = con_ser
constraints['rh']  = con_rh
constraints['rm']  = con_rm
constraints['rl']  = con_rl
constraints['rvl'] = con_rvl

# Attractions
attractors = ['rds_shwy','rds_main','cp_tc','angels_bnd']
att_ind = {'rds_shwy':[[0,20],[500,10],[10000,0]],'rds_main':[[0,15],[500,5],[5000,0]],'cp_tc':[[0,25],[1000,0]],'angels_bnd':[[0,30],[1000,0]]}
att_ret  = {'rds_shwy':[[0,25],[100,10],[10000,0]],'rds_main':[[0,15],[500,5],[5000,0]],'cp_tc':[[0,25],[1000,0]],'angels_bnd':[[0,30],[1000,0]]}
att_ser  = {'rds_shwy':[[0,25],[200,10],[10000,0]],'rds_main':[[0,15],[500,5],[5000,0]],'cp_tc':[[0,25],[1000,0]],'angels_bnd':[[0,30],[1000,0]]}
att_rh  = {'rds_shwy':[[0,20],[200,10],[10000,0]],'rds_main':[[0,15],[500,5],[5000,0]],'cp_tc':[[0,25],[1000,0]],'angels_bnd':[[0,30],[1000,0]]}
att_rm  = {'rds_shwy':[[0,20],[1000,10],[10000,0]],'rds_main':[[0,15],[1000,10],[10000,0]],'cp_tc':[[0,25],[1000,0]],'angels_bnd':[[0,30],[1000,0]]}
att_rl  = {'rds_shwy':[[0,10],[25000,0]],'rds_main':[[0,10],[25000,0]],'cp_tc':[[0,0]],'angels_bnd':[[0,0]]}
att_rvl = {'rds_shwy':[[0,5],[50000,0]],'rds_main':[[0,10],[25000,0]],'cp_tc':[[0,0]],'angels_bnd':[[0,0]]}

att_weights = {}
att_weights['ind'] = att_ind
att_weights['ret'] = att_ret
att_weights['ser'] = att_ser
att_weights['rh']  = att_rh
att_weights['rm']  = att_rm
att_weights['rl']  = att_rl
att_weights['rvl'] = att_rvl


Logger("Workspace Settings")
# Workspace Settings
arcpy.env.workspace = os.path.join(dbpath,fds)
arcpy.env.overwriteOutput = True

# GP Intersection
Logger("Parcel-GP")
arcpy.SpatialJoin_analysis('pcl_cent', 'gp', 'pcl_gp')



# Constraints
Logger("Prepping Constraints")
Logger("Unioning Constraints")
arcpy.Union_analysis(['pcl_bnd'] + con_list, 'in_memory\constraints_union')
Logger("Adding Fields")
arcpy.AddField_management('in_memory\constraints_union','acres_p',"DOUBLE")
arcpy.AddField_management('in_memory\constraints_union','pct_primary',"DOUBLE")
Logger("Calculating Fields")
arcpy.CalculateField_management('in_memory\constraints_union','acres_p','!shape.area@ACRES!','PYTHON_9.3')
arcpy.CalculateField_management('in_memory\constraints_union','pct_primary','!acres_p!/!acres!','PYTHON_9.3')

# Make Feature Layer
arcpy.MakeFeatureLayer_management('in_memory\constraints_union', "const_lyr","","in_memory",)


Logger("Computing Constraints")
for lu in LUPriorities:
    # All of this should be done in memory not as gp commands
    Logger("  Working on:{lu}".format(lu=lu))
    cons_sum_list = [] 
    arcpy.AddField_management('const_lyr','con_{lu}'.format(lu=lu),"DOUBLE")
    arcpy.AddField_management('const_lyr','dev_ac_{lu}'.format(lu=lu),"DOUBLE")
    arcpy.CalculateField_management('const_lyr','con_{lu}'.format(lu=lu),'0','PYTHON_9.3')
    arcpy.CalculateField_management('const_lyr','dev_ac_{lu}'.format(lu=lu),'0','PYTHON_9.3')
    for const in con_list:    
        cons_sum_list.append('!con_{lu}_{const}!'.format(lu=lu,const=const))
        arcpy.AddField_management('const_lyr','con_{lu}_{const}'.format(lu=lu,const=const),"DOUBLE")
        arcpy.CalculateField_management('const_lyr','con_{lu}_{const}'.format(lu=lu,const=const),'0','PYTHON_9.3')
        # arcpy.AddField_management('constraints_union','eff_con_{lu}_{const}'.format(lu=lu,const=const),"DOUBLE")
        arcpy.SelectLayerByAttribute_management('const_lyr','NEW_SELECTION','"{const}" = 1'.format(const=const))
        arcpy.CalculateField_management('const_lyr','con_{lu}_{const}'.format(lu=lu,const=const),'!{const}! * {val}'.format(val=constraints[lu][const],const=const),'PYTHON_9.3')
        arcpy.SelectLayerByAttribute_management('const_lyr','CLEAR_SELECTION')
        # arcpy.CalculateField_management('constraints_union','eff_con_{lu}_{const}'.format(lu=lu,const=const),constraints[lu][const],'PYTHON_9.3')
    arcpy.CalculateField_management('const_lyr','con_{lu}'.format(lu=lu)," + ".join(cons_sum_list),'PYTHON_9.3')
    # Make sure you don't have constraints over 100%
    arcpy.SelectLayerByAttribute_management('const_lyr','NEW_SELECTION','"con_{lu}" > 1.0'.format(lu=lu))
    arcpy.CalculateField_management('const_lyr','con_{lu}'.format(lu=lu),"1.0",'PYTHON_9.3')
    arcpy.SelectLayerByAttribute_management('const_lyr','CLEAR_SELECTION')
    
    # Calculate Developable area for the land use in each subpoly
    arcpy.CalculateField_management('const_lyr','dev_ac_{lu}'.format(lu=lu),'(1.0 - !con_{lu}!) * !acres_p!'.format(lu=lu),'PYTHON_9.3')
    
arcpy.CopyFeatures_management('const_lyr','constraints_union')

# Sum Developable Space for each Land Use

Logger("Summing Developable Areas")
sumspec = []
for lu in LUPriorities:
    sumspec.append(["dev_ac_{lu}".format(lu=lu),"SUM"])
arcpy.Statistics_analysis("constraints_union", os.path.join(dbpath,"sum_devspace"), sumspec, "pclid")

# Join sum_devspace to pcl_gp
Logger("Joining Devspace and gp")
flds = []
for lu in LUPriorities:
    flds.append("SUM_dev_ac_{lu}".format(lu=lu))
arcpy.JoinField_management("pcl_gp", "pclid", os.path.join(dbpath,"sum_devspace"), "pclid",flds)

# Set zero to parcels that don't allow land uses based on GP
Logger("GP Selection")
#arcpy.CopyFeatures_management('pcl_gp',r'in_memory\finalds')
arcpy.MakeFeatureLayer_management(r'pcl_gp', "pcl_gp_lyr")
for lu in LUPriorities:
    Logger("  Working on:{lu}".format(lu=lu))
    whereclause = """ "gp_class" NOT IN ('{gpvals}') """.format(gpvals = "','".join(gplu[lu]))
    calcfld = "SUM_dev_ac_{lu}".format(lu=lu)
    arcpy.SelectLayerByAttribute_management('pcl_gp_lyr','NEW_SELECTION',whereclause)
    arcpy.CalculateField_management('pcl_gp_lyr',calcfld,'0','PYTHON_9.3')
    arcpy.SelectLayerByAttribute_management('pcl_gp_lyr','CLEAR_SELECTION')

Logger("Calc Distances")
for att in attractors:
    Logger("  Working on:{att}".format(att=att))
    Logger("    Generate Near Table")
    arcpy.GenerateNearTable_analysis('pcl_gp_lyr', att, os.path.join(dbpath,"dist_{att}".format(att=att)), "", "", "", "CLOSEST")
    Logger("    Add Field")
    arcpy.AddField_management('pcl_gp_lyr','dist_{att}'.format(att=att),"DOUBLE")
    arcpy.CalculateField_management('pcl_gp_lyr','dist_{att}'.format(att=att),'0','PYTHON_9.3')
    Logger("    Add Join")
    arcpy.AddIndex_management (os.path.join(dbpath,"dist_{att}".format(att=att)), "IN_FID", "IN_FID_Idx", "UNIQUE", "ASCENDING")
    arcpy.AddJoin_management('pcl_gp_lyr','OBJECTID',os.path.join(dbpath,"dist_{att}".format(att=att)),'IN_FID','KEEP_ALL')
    Logger("    Calc Distance Field")
    arcpy.CalculateField_management('pcl_gp_lyr','dist_{att}'.format(att=att),'!{tname}.NEAR_DIST!'.format(tname="dist_{att}".format(att=att)),'PYTHON_9.3')
    arcpy.RemoveJoin_management('pcl_gp_lyr',"dist_{att}".format(att=att))
    



Logger("Make Weights")
for lu in LUPriorities:
    # Logger("Working on:{lu}".format(lu=lu))
    att_wt_list = []
    for att in attractors:
        Logger("  Working on: {lu}-{att}".format(att=att, lu=lu)) 
        arcpy.AddField_management('pcl_gp_lyr','wt_{lu}_{att}'.format(att=att, lu=lu),"DOUBLE")
        arcpy.CalculateField_management('pcl_gp_lyr','wt_{lu}_{att}'.format(att=att, lu=lu),'0','PYTHON_9.3')
        att_wt_list.append('!wt_{lu}_{att}!'.format(att=att, lu=lu))
        attlist = att_weights[lu][att]
        if attlist == [[0,0]]:
            # Attractor does not apply to this land use
            pass
        elif len(attlist) == 1:
            # Singular Value
            # TODO: implement this
            pass
        else:
            for b in range(0,len(attlist)-1):
                sdist   = attlist[b][0]
                sweight = attlist[b][1]
                edist   = attlist[b+1][0]
                eweight   = attlist[b+1][1]               
                #print(sdist, sweight, edist, eweight)
                whereclause = """dist_{att}  >= {sdist} AND dist_{att} < {edist}""".format(att=att, sdist=str(sdist), edist=str(edist))
                #print(whereclause)
                arcpy.SelectLayerByAttribute_management('pcl_gp_lyr','NEW_SELECTION',whereclause)
                m = (float(eweight) - float(sweight))/(float(edist)-float(sdist))
                calcstr = """{slope}*(!dist_{att}! - {sdist}) + {sweight}""".format(slope = str(round(m,5)),att=att, sdist = str(sdist), sweight=str(sweight), edist= str(edist), eweight=str(eweight))
                #print(calcstr)
                arcpy.CalculateField_management('pcl_gp_lyr','wt_{lu}_{att}'.format(att=att, lu=lu),calcstr,'PYTHON_9.3')
                arcpy.SelectLayerByAttribute_management('pcl_gp_lyr','CLEAR_SELECTION')
    arcpy.AddField_management('pcl_gp_lyr','wt_{lu}'.format(lu=lu),"DOUBLE")
    arcpy.CalculateField_management('pcl_gp_lyr','wt_{lu}'.format(lu=lu),'0','PYTHON_9.3')
    arcpy.CalculateField_management('pcl_gp_lyr','wt_{lu}'.format(lu=lu)," + ".join(att_wt_list),'PYTHON_9.3')
                
        


# loop land uses
Logger("Allocating Land Uses")
arcpy.AddField_management('pcl_gp_lyr','alloc',"TEXT","","",25)
arcpy.CalculateField_management('pcl_gp_lyr','alloc',"''",'PYTHON_9.3')
for lu in LUPriorities:
    Logger("  Working on:{lu}".format(lu=lu))
    targetAc = LUDemand[lu]
    # Select where alloc = "" and dev_ac_lu >0 Sort by wt_lu (desc)
    devAcFieldName = 'SUM_dev_ac_{lu}'.format(lu = lu)
    wtLUFieldName = 'wt_{lu}'.format(lu = lu)
    fieldnames = ['alloc',devAcFieldName, wtLUFieldName]
    orderby = """ ORDER BY wt_{lu} DESC""".format(lu=lu)
    whereclause = """ alloc = '' AND SUM_dev_ac_{lu} > 0 """.format(lu=lu)
    acTot = 0.0
    wt = None
    
    #for row in arcpy.da.SearchCursor("pcl_gp_lyr",fieldnames, where_clause = whereclause, sql_clause=(None, orderby)):
    for row in arcpy.da.SearchCursor("pcl_gp_lyr",fieldnames, where_clause = whereclause, sql_clause=(None, orderby)):
        acTot = acTot + row[1]
        if acTot >= targetAc:
            # Stop looping
            wt = row[2]
            break
    
    if wt == None:
        # not enough space allocate all
        where = whereclause
        arcpy.SelectLayerByAttribute_management('pcl_gp_lyr','NEW_SELECTION',where)
        arcpy.CalculateField_management('pcl_gp_lyr','alloc',"'"+lu+"'",'PYTHON_9.3')
        Logger("    Land Use is underallocated:" + str(targetAc - acTot))
        arcpy.SelectLayerByAttribute_management('pcl_gp_lyr','CLEAR_SELECTION')
    else:
        # Allocate all with wt >= wt
        where = whereclause + " AND " + wtLUFieldName + " >= " + str(wt)
        arcpy.SelectLayerByAttribute_management('pcl_gp_lyr','NEW_SELECTION',where)
        arcpy.CalculateField_management('pcl_gp_lyr','alloc',"'"+lu+"'",'PYTHON_9.3')
        Logger("    Full Allocation")
        arcpy.SelectLayerByAttribute_management('pcl_gp_lyr','CLEAR_SELECTION')
    
#arcpy.CopyFeatures_management('in_memory\finalds', 'finalalloc')    
    
    

Logger("UPlan Complete")