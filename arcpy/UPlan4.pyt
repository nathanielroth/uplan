import arcpy
import os
import cPickle as pickle
import UPConfig as upc
import UIUtilities as uiut
from operator import itemgetter
import Utilities
import CalcSubarea
import CalcGeneralPlans
import CalcConstraints
import CalcWeights
import Allocation
from UIUtilities import SplitPath

class Toolbox(object):
    def __init__(self):
        """Define the toolbox (the name of the toolbox is the name of the
        .pyt file)."""
        self.label = "UPlan 4"
        self.alias = ""

        # List of tool classes associated with this toolbox
        self.tools = [CreateUPlanGDB, ImportBaseGeometry, ImportConstraint, ImportAttractor, ImportGeneralPlan, ImportSALayer,
                      MakePickle,AddTimeStep,RemoveTimeStep,AddLandUse,SetLandUsePriority,RemoveLandUse,
                      SetConstraints,SetConstraintWeights,SetAttractors,SetAttractorWeights,SetGeneralPlan,SetGeneralPlanSettings,SetMixedUse,
                      PreCalcAll,PreCalcSubareas,PreCalcGeneralPlans,PreCalcConstraints,PreCalcWeights,RunAllocation,ZonalSummary]


class CreateUPlanGDB(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "1. Create UPlan Geodatabase"
        self.description = "This tool creates a geodatabase for all UPlan layers and tables"
        self.canRunInBackground = False
        self.category = "A. Data Loading"

    def getParameterInfo(self):
        """Define parameter definitions"""
        # First parameter
        UPlan_gdb_folder = arcpy.Parameter(
            displayName="Location for UPlan GDB",
            name="uplan_gdb_folder",
            datatype="DEFolder",
            parameterType="Required",
            direction="Input")
        
         # Second parameter
        UPlan_gdb_name = arcpy.Parameter(
            displayName="Name for UPlan GDB",
            name="uplan_gdb_name",
            datatype="GPString",
            parameterType="Required",
            direction="Input")
          
        
        params = [UPlan_gdb_folder, UPlan_gdb_name]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        Uplan_gdb_folder = parameters[0].valueAsText
        Uplan_gdb_name = parameters[1].valueAsText
       
        #create UPlan geodatabase
        arcpy.CreateFileGDB_management(Uplan_gdb_folder, Uplan_gdb_name)
        
        #create UPC_*tables
        UPConfig.EmptyOrCreateUPCTables(os.path.join(Uplan_gdb_folder, Uplan_gdb_name) + '.gdb')                            
        return


class ImportBaseGeometry(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "2. Import Base Geometry Layer"
        self.description = ""
        self.canRunInBackground = False
        self.category = "A. Data Loading"

    def getParameterInfo(self):
        """Define parameter definitions"""
        # First parameter
        in_base_geom_layer = arcpy.Parameter(
            displayName="Base Geometry Layer",
            name="geom_layer",
            datatype="GPFeatureLayer",
            parameterType="Required",
            direction="Input")
                               
        #Second parameter
        Descriptive_name=arcpy.Parameter(
            displayName="Base Geometry Descriptive Name",
            name="desc_name",
            datatype="GPString",
            parameterType="Required",
            direction="Input") 
        
        #Third parameter
        UPlan_geodatabase=arcpy.Parameter(
            displayName="UPlan File Geodatabase",
            name="up_gdb",
            datatype="DEWorkspace",
            parameterType="Required",
            direction="Input")  
        
        
        params = [in_base_geom_layer, Descriptive_name, UPlan_geodatabase]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        in_layer = parameters[0].valueAsText
        desc_name = parameters[1].valueAsText
        Uplan_gdb = parameters[2].valueAsText
        
        
        LayerManagement.ImportBaseGeom(Uplan_gdb, in_layer, desc_name)
                       
    
        return

class ImportConstraint(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "3. Import Constraint Layer"
        self.description = "This tool imports a constraint layer to the UPlan geodatabase"
        self.canRunInBackground = False
        self.category = "A. Data Loading"

    def getParameterInfo(self):
        """Define parameter definitions"""
        # First parameter
        InConstraint = arcpy.Parameter(
            displayName="Constraint Layer",
            name="constraint_layer",
            datatype="GPFeatureLayer",
            parameterType="Required",
            direction="Input")
        
        #Second parameter
        LongName=arcpy.Parameter(
            displayName="Constraint Layer Long Name",
            name="desc_name",
            datatype="GPString",
            parameterType="Required",
            direction="Input") 
                                
         #Third parameter
        UPlan_geodatabase=arcpy.Parameter(
            displayName="UPlan File Geodatabase",
            name="up_gdb",
            datatype="DEWorkspace",
            parameterType="Required",
            direction="Input")      
                                  

        params = [InConstraint, LongName, UPlan_geodatabase]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        in_layer = parameters[0].valueAsText
        Uplan_gdb = parameters[2].valueAsText
        desc_name = parameters[1].valueAsText
        
        LayerManagement.ImportConstraintLayer(in_layer, Uplan_gdb, desc_name)
                       
    
        return
    
class ImportAttractor(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "4. Import Attractor Layer"
        self.description = "This tool imports an attractor layer to the UPlan geodatabase"
        self.canRunInBackground = False
        self.category = "A. Data Loading"

    def getParameterInfo(self):
        """Define parameter definitions"""
        # First parameter
        InAttractor = arcpy.Parameter(
            displayName="Attractor Layer",
            name="attractor_layer",
            datatype="GPFeatureLayer",
            parameterType="Required",
            direction="Input")
        
        #Second parameter
        LongName=arcpy.Parameter(
            displayName="Attractor Layer Long Name",
            name="desc_name",
            datatype="GPString",
            parameterType="Required",
            direction="Input") 
                                
         #Third parameter
        UPlan_geodatabase=arcpy.Parameter(
            displayName="UPlan File Geodatabase",
            name="up_gdb",
            datatype="DEWorkspace",
            parameterType="Required",
            direction="Input")      
                                  

        params = [InAttractor, LongName, UPlan_geodatabase]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        in_layer = parameters[0].valueAsText
        Uplan_gdb = parameters[2].valueAsText
        desc_name = parameters[1].valueAsText
        
        LayerManagement.ImportAttractorLayer(in_layer, Uplan_gdb, desc_name)
                       
    
        return
    
class ImportGeneralPlan(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "5. Import General Plan Layer"
        self.description = "This tool imports a general plan layer to the UPlan geodatabase"
        self.canRunInBackground = False
        self.category = "A. Data Loading"

    def getParameterInfo(self):
        """Define parameter definitions"""
        # First parameter
        InGP = arcpy.Parameter(
            displayName="General Plan Layer",
            name="gp_layer",
            datatype="GPFeatureLayer",
            parameterType="Required",
            direction="Input")
        
        #Second parameter
        LongName=arcpy.Parameter(
            displayName="General Plan Layer Descriptive Name",
            name="desc_name",
            datatype="GPString",
            parameterType="Required",
            direction="Input") 
        
        #Third parameter
        GPCatField=arcpy.Parameter(
            displayName="Field within the layer that contains the general plan codes",
            name="gp_cat_field",
            datatype="Field",
            parameterType="Required",
            direction="Input")  
        GPCatField.parameterDependencies = [InGP.name]
                                
         #Fourth parameter
        UPlan_geodatabase=arcpy.Parameter(
            displayName="UPlan File Geodatabase",
            name="up_gdb",
            datatype="DEWorkspace",
            parameterType="Required",
            direction="Input")      
                                  

        params = [InGP, LongName, GPCatField, UPlan_geodatabase]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        in_layer = parameters[0].valueAsText
        Uplan_gdb = parameters[3].valueAsText
        desc_name = parameters[1].valueAsText
        gp_field = parameters[2].valueAsText
          
        LayerManagement.ImportGPLayer(in_layer, Uplan_gdb, gp_field, desc_name)
                       
    
        return
    
class ImportSALayer(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "6. Import SubArea Layer"
        self.description = "Imports an SubArea layer to the geodatabase"
        self.canRunInBackground = False
        self.category = "A. Data Loading"

    def getParameterInfo(self):
        """Define parameter definitions"""
        # First parameter
        InSALayer = arcpy.Parameter(
            displayName="SubArea Layer",
            name="subarea_layer",
            datatype="GPFeatureLayer",
            parameterType="Required",
            direction="Input")
        
        #Second parameter
        LongName=arcpy.Parameter(
            displayName="SubArea Layer Descriptive Name",
            name="desc_name",
            datatype="GPString",
            parameterType="Required",
            direction="Input") 
        
        #Third parameter
        SAIDField=arcpy.Parameter(
            displayName="Field within the layer that contains the SubArea codes (or IDs)",
            name="sa_id_field",
            datatype="Field",
            parameterType="Required",
            direction="Input")  
        SAIDField.parameterDependencies = [InSALayer.name]
                
        #Fourth parameter
        SANameField=arcpy.Parameter(
            displayName="Field within the layer that contains the SubArea descriptions (or Names)",
            name="sa_name_field",
            datatype="Field",
            parameterType="Required",
            direction="Input")  
        SANameField.parameterDependencies = [InSALayer.name]
                                
         #Fifth parameter
        UPlan_geodatabase=arcpy.Parameter(
            displayName="UPlan File Geodatabase",
            name="up_gdb",
            datatype="DEWorkspace",
            parameterType="Required",
            direction="Input") 

        #Sixth parameter        
        SearchLength=arcpy.Parameter(
            displayName="Maximum distance between centroids and edge of SubArea",
            name="search_length",
            datatype="GPLinearUnit",
            parameterType="Required",
            direction="Input")      
                                  

        params = [InSALayer, UPlan_geodatabase, LongName, SAIDField, SANameField, SearchLength]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        InSALayer = parameters[0].valueAsText
        UPlanGDB = parameters[1].valueAsText
        LongName = parameters[2].valueAsText
        SAIDField = parameters[3].valueAsText
        SANameField = parameters[4].valueAsText
        SearchLength = parameters[5].valueAsText
        
        messages.addMessage("SA Code: {sanf}".format(sanf = SAIDField))
        messages.addMessage("SA Long Name: {sanf}".format(sanf = SANameField))
        
        messages = LayerManagement.ImportSALayer(InSALayer, UPlanGDB, LongName, SAIDField, SANameField, SearchLength, messages)
        
       
                       
    
        return

class MakePickle(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "1. Make Pickle"
        self.description = ""
        self.canRunInBackground = False
        self.category = "B. Model Configuration"

    def getParameterInfo(self):
        """Define parameter definitions"""
        pUPGDB = arcpy.Parameter(
            displayName="UPlan geodatabase",
            name="UPGDB",
            datatype="DEWorkspace",
            parameterType="Required",
            direction="Input")
        params = [pUPGDB]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        if parameters[0].value:
            if uiut.CheckIfFGDB(parameters[0].value)==False:
                parameters[0].setErrorMessage("You must select a file geodatabase")
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        UPGDB = parameters[0].valueAsText
        splitpath = uiut.SplitPath(UPGDB)
        messages.addMessage("Reading Configuration from Database")
        UPConfig = upc.ReadUPConfigFromGDB(splitpath[0],splitpath[1])
        messages.addMessage("Writing Pickle")
        uiut.MakePickle(UPConfig, UPGDB)
        messages.addMessage("Pickle Written")
        
        
        return

class RemoveTimeStep(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "3. Remove a Time Step"
        self.description = "Remove a Time Step from the configuration. Note, this cannot be undone and removes all traces of the time step."
        self.canRunInBackground = False
        self.category = "B. Model Configuration"

    def getParameterInfo(self):
        """Define parameter definitions"""
        
        pUPGDB = arcpy.Parameter(
            displayName="UPlan geodatabase",
            name="UPGDB",
            datatype="DEWorkspace",
            parameterType="Required",
            direction="Input")
        
        pTSShortName = arcpy.Parameter(
            displayName="Time Step Short Name",
            name="TSShortName",
            datatype="GPString",
            parameterType="Required",
            direction="Input")
        pTSShortName.filter.list = ["Select a UPlan Geodatabase to get a list of time steps"]
        
        params = [pUPGDB,pTSShortName]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        if parameters[0].value:
            upgdb = parameters[0].valueAsText
            picklepath = "\\".join([upgdb,"UPConfig.p"])
            UPConfig = uiut.LoadPickle(picklepath)
            parameters[1].filter.list = [ts[0] for ts in UPConfig['TimeSteps']]
            

        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        if parameters[0].value:
            if uiut.CheckIfFGDB(parameters[0].value)==False:
                parameters[0].setErrorMessage("You must select a file geodatabase")
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        
        upgdb = parameters[0].valueAsText
        ts = parameters[1].valueAsText
        
        picklepath = "\\".join([upgdb,"UPConfig.p"])
        UPConfig = uiut.LoadPickle(picklepath)
        
        # Remove the timestep
        
        UPConfig.pop(ts,None)
        
        for TimeStep in UPConfig['TimeSteps']:
            if TimeStep[0] == ts:
                UPConfig['TimeSteps'].remove(TimeStep)
                messages.addMessage("Removed Timestep: {ts},{tsln}".format(ts=TimeStep[0],tsln=TimeStep[1]))
                break
         # Write out the pickle
        uiut.MakePickle(UPConfig, upgdb)
        
        # Write out to tables
        upc.WriteUPConfigToGDB(UPConfig)
        
        return

class AddTimeStep(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "2. Add A Timestep"
        self.description = "Add a Timestep"
        self.canRunInBackground = False
        self.category = "B. Model Configuration"

    def getParameterInfo(self):
        """Define parameter definitions"""
        pUPGDB = arcpy.Parameter(
            displayName="UPlan geodatabase",
            name="UPGDB",
            datatype="DEWorkspace",
            parameterType="Required",
            direction="Input")
        
        pTSLongName = arcpy.Parameter(
            displayName="Time Step Long Name",
            name="TSLongName",
            datatype="GPString",
            parameterType="Required",
            direction="Input")
        
        pTSShortName = arcpy.Parameter(
            displayName="Time Step Short Name",
            name="TSShortName",
            datatype="GPString",
            parameterType="Required",
            direction="Input")
        
        pTSPosition = arcpy.Parameter(
            displayName="Time Step Position",
            name="TSPosition",
            datatype="GPLong",
            parameterType="Required",
            direction="Input")
        
        params = [pUPGDB,pTSLongName,pTSShortName,pTSPosition]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        if parameters[0].value:
            if uiut.CheckIfFGDB(parameters[0].value)==False:
                parameters[0].setErrorMessage("You must select a file geodatabase")
        
            if parameters[2].value:
                if uiut.CheckIfTSInFGDB(str(parameters[0].value), str(parameters[2].value)):
                    parameters[2].setErrorMessage("The short name must be unique. See the upc_timesteps table.")
                    
            if parameters[3].value:
                if int(parameters[3].value) < 1:
                    parameters[3].setErrorMessage("Position must be at least 1")
                  
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        # For running 
        UPGDB = parameters[0].valueAsText
        TSLongName = parameters[1].valueAsText
        TSShortName = parameters[2].valueAsText
        TSPosition = int(parameters[3].valueAsText)
        # for debug
#         UPGDB = r"C:\Projects\UPlan4\testing\calaveras.gdb"
#         TSLongName = "Time Step 3"
#         TSShortName = "ts3"
#         TSPosition = 3
        
        messages.addMessage("Adding Time Step")
        # Check for Pickle
        picklepath =  "\\".join([UPGDB,"UPConfig.p"])
        if uiut.CheckForPickle(picklepath):
            UPConfig = uiut.LoadPickle(picklepath)
            # add to TimeStep list in the correct order
            TimeSteps = UPConfig['TimeSteps']
            UPConfig['TimeSteps'] = uiut.InsertToList(TimeSteps, [TSShortName,TSLongName], TSPosition)
            
            # Add empty TS dictionary to hold everything else. 
            UPConfig[TSShortName] = uiut.MakeEmptyTimeStep()
            
            
            # Write out the pickle
            uiut.MakePickle(UPConfig, UPGDB)
            
            # Write out to tables
            upc.WriteUPConfigToGDB(UPConfig)
            
            messages.addMessage("Time step added, remember to configure the related attractors, discouragers, generalplans, etc..")
            print("Time step added.")
        else:
            messages.addMessage("Unable To Add Time Step")
            print("Unable to add time step")
            
        return

class AddLandUse(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "4. Add Land Use"
        self.description = ""
        self.canRunInBackground = False
        self.category = "B. Model Configuration"

    def getParameterInfo(self):
        """Define parameter definitions"""
        pUPGDB = arcpy.Parameter(
            displayName="UPlan geodatabase",
            name="UPGDB",
            datatype="DEWorkspace",
            parameterType="Required",
            direction="Input")
        
        pLUShortName = arcpy.Parameter(
            displayName="Land Use Short Name",
            name="LUShortName",
            datatype="GPString",
            parameterType="Required",
            direction="Input")
        
        pLULongName = arcpy.Parameter(
            displayName="Land Use Long Name",
            name="LULongName",
            datatype="GPString",
            parameterType="Required",
            direction="Input")
        
        pLUType = arcpy.Parameter(
            displayName="Land Use Type",
            name="LUType",
            datatype="GPString",
            parameterType="Required",
            direction="Input")
        pLUType.filter.list = ["Residential","Employment"]
        
        pLUAlloc = arcpy.Parameter(
            displayName="Allocation Mode",
            name="LUAlloc",
            datatype="GPString",
            parameterType="Required",
            direction="Input")
        pLUAlloc.filter.list = ["Normal","Random"]
        
        
        params = [pUPGDB,pLUShortName,pLULongName,pLUType]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        
        if parameters[0].value:
            if uiut.CheckIfFGDB(parameters[0].value)==False:
                parameters[0].setErrorMessage("You must select a file geodatabase")
            else:
                picklepath = "\\".join([upgdb,"UPConfig.p"])
                UPConfig = uiut.LoadPickle(picklepath)
                if parameters[1] in UPConfig['LUPriority']:
                    parameters[1].setErrorMessage("The land use short name already exists")
        
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        # For running 
        pUPGDB = parameters[0].valueAsText
        pLUShortName = parameters[1].valueAsText
        pLULongName = parameters[2].valueAsText
        pLUType = parameters[3].valueAsText
        pLUAlloc = parameters[4].valueAsText
        # for debug
#         pUPGDB = parameters[0].valueAsText
#         pLUShortName = parameters[1].valueAsText
#         pLULongName = parameters[2].valueAsText
#         pLUType = parameters[3].valueAsText
        
        messages.addMessage("Adding Land Use")
        # Check for Pickle
        picklepath =  "\\".join([UPGDB,"UPConfig.p"])
        if uiut.CheckForPickle(picklepath):
            UPConfig = uiut.LoadPickle(picklepath)
            # add to TimeStep list in the correct order
            UPConfig['LUPriority'].append(pLUShortName)
            
            UPConfig['LUNames'][pLUShortName] = pLULongName
            
            if pLUAlloc == 'Normal':
                UPConfig['AllocMethods'][pLUShortName] = 1
            elif pLUAlloc == 'Random':
                UPConfig['AllocMethods'][pLUShortName] = 2
            else:
                messages.addError("Unrecognized Allocation Type (Normal or Random)")
                
            if pLUType == "Residential":
                UPConfig['LUTypes'][pLUShortName] = "res"
            elif pLUType == "Employment":
                UPConfig['LUTypes'][pLUShortName] = "emp"
            else:
                messages.addError("Unrecognized Land Use Type (Residentia or Employment)")
                
            # Add empty TS dictionary to hold everything else.
            for TimeStep in UPConfig['TimeSteps']:
                ts = TimeStep[0]
                
                # Empty Demand
                for SubArea in UPConfig['Subareas']:
                    sa = SubArea['sa']
                    UPConfig[ts]['Demand'][sa] = 0.0
                
                # General plan
                UPConfig[ts]['gplu'][pLUShortName] = []
                
                # Constraints
                UPConfig[ts]['cweights'][pLUShortName] = {}
                
                # Attractors
                UPConfig[ts]['aweights'][pLUShortName] = {}
                 

            
            
            # Write out the pickle
            uiut.MakePickle(UPConfig, pUPGDB)
            
            # Write out to tables
            upc.WriteUPConfigToGDB(UPConfig)
            
            messages.addMessage("Land use added, remember to configure the related attractors, discouragers, generalplans, etc..")
            print("Land use added.")
        else:
            messages.addMessage("Unable To Add Land Use")
            print("Unable to Remove Land Use")
            
        return

class RemoveLandUse(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "5. Remove Land Use"
        self.description = ""
        self.canRunInBackground = False
        self.category = "B. Model Configuration"

    def getParameterInfo(self):
        """Define parameter definitions"""
        
        pUPGDB = arcpy.Parameter(
            displayName="UPlan geodatabase",
            name="UPGDB",
            datatype="DEWorkspace",
            parameterType="Required",
            direction="Input")
        
        pLUShortName = arcpy.Parameter(
            displayName="Land Use Short Name",
            name="LUShortName",
            datatype="GPString",
            parameterType="Required",
            direction="Input")
        params = [pUPGDB,pLUShortName]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        if parameters[0].value:
            if uiut.CheckIfFGDB(parameters[0].value)==False:
                parameters[0].setErrorMessage("You must select a file geodatabase")
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        pUPGDB = parameters[0].valueAsText
        pLUShortName = parameters[1].valueAsText
        
        picklepath =  "\\".join([pUPGDB,"UPConfig.p"])
        if uiut.CheckForPickle(picklepath):
            UPConfig = uiut.LoadPickle(picklepath)
            
            for TimeStep in UPConfig['TimeSteps']:
                ts = TimeStep[0]
                
                # Empty Demand
                for SubArea in UPConfig['Subareas']:
                    sa = SubArea['sa']
                    del UPConfig[ts]['Demand'][sa][pLUShortName] 
                    
                # General Plan
                del UPConfig[ts]['gplu'][pLUShortName]
                
                # Constraints
                del UPConfig[ts]['cweights'][pLUShortName]
                
                # Attractors
                del UPConfig[ts]['aweights'][pLUShortName]
            
            # long name listing
            del UPConfig['LUNames'][pLUShortName]
            
            # LU Type
            del UPConfig['LUTypes'][pLUShortName]
            
            # Allocation Type
            del UPConfig['AllocMethods'][pLUShortName]
            
            # LU Priority
            UPConfig['LUPriority'].remove(pLUShortName)
            
            # Write out the pickle
            uiut.MakePickle(UPConfig, pUPGDB)
            
            # Write out to tables
            upc.WriteUPConfigToGDB(UPConfig)
            
            messages.addMessage("Land use removed")
            print("Land use added.")
        else:
            messages.addMessage("Unable To Remove Land Use")
            print("Unable to Remove Land Use")
            
        return

class SetLandUsePriority(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "6. Set Land Use Priority"
        self.description = "Reorder Landuses"
        self.canRunInBackground = False
        self.category = "B. Model Configuration"

    def getParameterInfo(self):
        """Define parameter definitions"""
        
        pUPGDB = arcpy.Parameter(
            displayName="UPlan geodatabase",
            name="UPGDB",
            datatype="DEWorkspace",
            parameterType="Required",
            direction="Input")
        
        pLU = arcpy.Parameter(
            displayName="Set Land Use Priority",
            name="Constraints",
            datatype="GPValueTable",
            parameterType="Required",
            direction="Input")
        pLU.columns = [['String', 'Land Use']]
        
        params = [pUPGDB,pLU]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        hasPickle = False
        
        
        
        if parameters[0].value:
            pUPGDB = parameters[0].valueAsText
            picklepath = "\\".join([pUPGDB,"UPConfig.p"])
            if uiut.CheckForPickle(picklepath):
                UPConfig = uiut.LoadPickle(picklepath)
                hasPickle = True
            
        
        if parameters[1].values and hasPickle:
            values = parameters[1].values
            luorder = [luo[0] for luo in values]
            UPConfig['PriorLUPrioirty'] = UPConfig['LUPriority']
            UPConfig['LUPriority'] = luorder
            uiut.MakePickle(UPConfig, pUPGDB)
                    
            
        if hasPickle:
            LandUseList = UPConfig['LUPriority']
            values = [[str(lu)] for lu in LandUseList]
            parameters[1].values = values

        
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        if parameters[0].value:
            if uiut.CheckIfFGDB(parameters[0].value)==False:
                parameters[0].setErrorMessage("You must select a file geodatabase")
                 
            else:
                picklepath =  "\\".join([str(parameters[0].valueAsText),"UPConfig.p"])
                if uiut.CheckForPickle(picklepath):
                    UPConfig = uiut.LoadPickle(picklepath)
                else:
                    parameters[0].setErrorMessage("UPlan configuration pickle missing or damaged: Please Run 'Make Pickle'")
        
        #TODO: Check this validation
        if parameters[1].values and UPConfig:
            values = parameters[1].values
            if 'PriorLUPriority' in UPConfig.keys():
                for value in values:
                    if str(value[0]) not in UPConfig['PriorLUPriority']:
                        parameters[1].setErrorMessage("The following land use is not recognize: {lu}".format(lu=value))
                
                for lu in UPConfig['PriorLUPriority']:
                    vals = [str(luv[0]) for luv in values]
                    if str(lu) not in vals:
                        parameters[1].setErrorMessage("The following land use is missing: {lu}".format(lu=str(lu)))
                
        
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        
        UPGDB = parameters[0].valueAsText
        
        picklepath =  "\\".join([UPGDB,"UPConfig.p"])
        
        if uiut.CheckForPickle(picklepath):
            UPConfig = uiut.LoadPickle(picklepath)
            values = parameters[1].values
            luorder = [luo[0] for luo in values]
            UPConfig['LUPriority'] = luorder
            if 'PriorLUPriority' in UPConfig.keys():
                del UPConfig['PriorLUPriority']
            
            # Write out the pickle
            uiut.MakePickle(UPConfig, UPGDB)
            
            # Write out to tables
            upc.WriteUPConfigToGDB(UPConfig)
        
        return
class SetConstraints(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Set Constraint Layers"
        self.description = "Set the list of constraints to be available for each time step"
        self.canRunInBackground = False
        self.category = "C. Scenario Settings"

    def getParameterInfo(self):
        """Define parameter definitions"""
        params = None
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        return


class SetConstraintWeights(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Set Constraint Weights"
        self.description = ""
        self.canRunInBackground = False
        self.category = "C. Scenario Settings"

    def getParameterInfo(self):
        """Define parameter definitions"""
        
        pUPGDB = arcpy.Parameter(
            displayName="UPlan geodatabase",
            name="UPGDB",
            datatype="DEWorkspace",
            parameterType="Required",
            direction="Input")
        
        pTimeStep = arcpy.Parameter(
            displayName="Select Time Step",
            name="TimeStep",
            datatype="GPString",
            parameterType="Required",
            direction="Input")
        pTimeStep.filter.list = ["Select your UPlan Geodatabase"]
        
        pLandUse = arcpy.Parameter(
            displayName="Select Land Use",
            name="LandUse",
            datatype="GPString",
            parameterType="Required",
            direction="Input")
        pLandUse.filter.list = ["Select your UPlan Geodatabase"]
        
        pConstraints = arcpy.Parameter(
            displayName="Set Constraint Weights",
            name="Constraints",
            datatype="GPValueTable",
            parameterType="Required",
            direction="Input")
        pConstraints.columns = [['String', 'Constraint'], ['Double', 'Weight']]
        
        
        
        
        
        
        params = [pUPGDB,pTimeStep,pLandUse,pConstraints]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        hasPickle = False
        if parameters[0].value:
            pUPGDB = parameters[0].valueAsText
            picklepath = "\\".join([pUPGDB,"UPConfig.p"])
            UPConfig = uiut.LoadPickle(picklepath)
            parameters[1].filter.list = [ts[0] for ts in UPConfig['TimeSteps']]
            parameters[2].filter.list = UPConfig['LUPriority']
            hasPickle = True
        
        hasTSandLU = False
        if parameters[1].value and parameters[2].value:
            ts = parameters[1].valueAsText
            lu = parameters[2].valueAsText
            hasTSandLU = True
        
        if parameters[3].value and hasPickle and hasTSandLU: # or should this be parameters[3].altered
            values = parameters[3].values
        
            for val in values:
                UPConfig[ts]['cweights'][lu][val[0]] = val[1]
            

            # Write out the pickle
            uiut.MakePickle(UPConfig, pUPGDB)

        
            
        if hasTSandLU and hasPickle:
            constraints = UPConfig[ts]['constraints']
            cweights = []
            for constraint in constraints:
                if constraint in UPConfig[ts]['cweights'][lu]:
                    cweight = [constraint,UPConfig[ts]['cweights'][lu][constraint]]
                else:
                    cweight = [constraint,0.0]
                cweights.append(cweight)
            
            parameters[3].values = cweights
            
            
            
            
            
        
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        if parameters[0].value:
            if uiut.CheckIfFGDB(parameters[0].value)==False:
                parameters[0].setErrorMessage("You must select a file geodatabase")
        
        if parameters[3].value:
            for val in parameters[3].value:
                if val[1] > 1.0 or val[1] < 0:
                    parameters[3].setErrorMessage("Weights must be between 0 and 1")
        
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        pUPGDB = parameters[0].valueAsText
        pLUShortName = parameters[1].valueAsText
        
        picklepath =  "\\".join([pUPGDB,"UPConfig.p"])
        if uiut.CheckForPickle(picklepath):
            UPConfig = uiut.LoadPickle(picklepath)
            ts = parameters[1].valueAsText
            lu = parameters[2].valueAsText
            
            
            values = parameters[3].values
            
            for val in values:
                UPConfig[ts]['cweights'][lu][val[0]] = val[1]
            

            # Write out the pickle
            uiut.MakePickle(UPConfig, pUPGDB)
            
            # Write out to tables
            upc.WriteUPConfigToGDB(UPConfig)
            
            messages.addMessage("Constraints Updated")
            print("Land use added.")
        else:
            messages.addMessage("Unable To Change Constraints")
            print("Unable to change constraints")
        
        
        return

class SetAttractors(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Set Attractor Layers"
        self.description = "Set the list of attractors to be available for each time step"
        self.canRunInBackground = False
        self.category = "C. Scenario Settings"

    def getParameterInfo(self):
        """Define parameter definitions"""
        
        pUPGDB = arcpy.Parameter(
            displayName="UPlan geodatabase",
            name="UPGDB",
            datatype="DEWorkspace",
            parameterType="Required",
            direction="Input")
        
        pTimeStep = arcpy.Parameter(
            displayName="Select Time Step",
            name="TimeStep",
            datatype="GPString",
            parameterType="Required",
            direction="Input")
        pTimeStep.filter.list = ["Select your UPlan Geodatabase"]
        
        
        pAttractors = arcpy.Parameter(
            displayName="Set Attractor Availablity",
            name="Attractor Availability",
            datatype="GPValueTable",
            parameterType="Required",
            direction="Input")
        pAttractors.columns = [['String', 'Layer'], ['String', 'Availability (Yes/No)']]
        
        params = [pUPGDB,pTimeStep,pAttractors]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        hasPickle = False
        if parameters[0].value:
            UPGDB = parameters[0].valueAsText
            picklepath = "\\".join([UPGDB,"UPConfig.p"])
            UPConfig = uiut.LoadPickle(picklepath)
            parameters[1].filter.list = [ts[0] for ts in UPConfig['TimeSteps']]
            hasPickle = True
            
        
        if parameters[2].values and parameters[1].value:
            values = parameters[2].values
            attlist = []
            for val in values:
                if val[1] == true:
                    attlist.append(val[0])
            UPConfig[parameters[1].valueAsText]['attractors'] = attlist
            attlist = None
        
        
        if parameters[1].value and hasPickle:
            # Get list of attraction layers
            attlist = []
            tclist = [str(tslyr) for tslyr in UPConfig[parameters[1].valueAsText]['attractors']]
            inlist = []
            whereClause = "Role = 'Attractor'"
            cur = arcpy.SearchCursor(os.path.join(UPGDB,"upc_layers"), whereClause, fields = "FCName;LongName")
            for row in cur:
                lyr = row.getValue("FCName")
                if str(lyr) not in inlist:
                    inlist.append(str(lyr))
                    if str(lyr) in tclist:
                        attlist.append([str(lyr),"Yes"])
                    else:
                        attlist.append([str(lyr),"No"])
            parameters[2].values = attlist
            
            
        
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        if parameters[0].value:
            if uiut.CheckIfFGDB(parameters[0].value)==False:
                parameters[0].setErrorMessage("You must select a file geodatabase")
        
        
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        hasPickle = False
        if parameters[0].value:
            UPGDB = parameters[0].valueAsText
            picklepath = "\\".join([UPGDB,"UPConfig.p"])
            UPConfig = uiut.LoadPickle(picklepath)
            parameters[1].filter.list = [ts[0] for ts in UPConfig['TimeSteps']]
            hasPickle = True
            
        
        if parameters[2].values and parameters[1].value:
            messages.addMessage("Updating Active Attractor List")
            values = parameters[2].values
            attlist = []
            for val in values:
                if str(val[1][:1]).lower() == "y":
                    attlist.append(val[0])
            UPConfig[parameters[1].valueAsText]['attractors'] = attlist
            attlist = None
            
            # Write out the pickle
            uiut.MakePickle(UPConfig, UPGDB)
            
            # Write out to tables
            upc.WriteUPConfigToGDB(UPConfig)
            messages.addMessage("Attractor List Updated")
            
        
        return

    
class SetAttractorWeights(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Set Attractor Weights"
        self.description = ""
        self.canRunInBackground = False
        self.category = "C. Scenario Settings"

    def getParameterInfo(self):
        """Define parameter definitions"""
        
        pUPGDB = arcpy.Parameter(
            displayName="UPlan geodatabase",
            name="UPGDB",
            datatype="DEWorkspace",
            parameterType="Required",
            direction="Input")
        
        pTimeStep = arcpy.Parameter(
            displayName="Select Time Step",
            name="TimeStep",
            datatype="GPString",
            parameterType="Required",
            direction="Input")
        pTimeStep.filter.list = ["Select your UPlan Geodatabase"]
        
        pLandUse = arcpy.Parameter(
            displayName="Select Land Use",
            name="LandUse",
            datatype="GPString",
            parameterType="Required",
            direction="Input")
        pLandUse.filter.list = ["Select your UPlan Geodatabase"]
        
        pAttractor = arcpy.Parameter(
            displayName="Set Attraction Layer",
            name="Attractor",
            datatype="GPString",
            parameterType="Required",
            direction="Input")
        pAttractor.filter.list = ["Select your UPlan Geodatabase and TimeStep"]
        
        pAWeights = arcpy.Parameter(
            displayName="Set Attraction Weights",
            name="Attraction Weights",
            datatype="GPValueTable",
            parameterType="Required",
            direction="Input")
        pAWeights.columns = [['Double', 'Distance'], ['Double', 'Weight']]
        
        params = [pUPGDB,pTimeStep,pLandUse,pAttractor,pAWeights]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        hasPickle = False
        if parameters[0].value:
            UPGDB = parameters[0].valueAsText
            picklepath = "\\".join([UPGDB,"UPConfig.p"])
            UPConfig = uiut.LoadPickle(picklepath)
            parameters[1].filter.list = [ts[0] for ts in UPConfig['TimeSteps']]
            parameters[2].filter.list = UPConfig['LUPriority']
            hasPickle = True
        
        hasTSandLU = False
        if parameters[1].value and parameters[2].value:
            ts = parameters[1].valueAsText
            lu = parameters[2].valueAsText
            hasTSandLU = True
            
        if hasTSandLU and hasPickle:
            # get attractors by land use
            parameters[3].filter.list = UPConfig[ts]['attractors']
            
        
        if parameters[4].value and parameters[3].value and hasPickle and hasTSandLU: # or should this be parameters[3].altered
            # this is a slightly ugly workaround to avoid loosing any changes that were just made.
            values = parameters[4].values
        
            atweights = [[val[0],val[1]] for val in values]
                
            UPConfig[ts]['aweights'][lu][parameters[3].valueAsText] = atweights
            
            # Write out the pickle
            uiut.MakePickle(UPConfig, UPGDB)

        # Load the attractors weights
        if parameters[3].value and hasTSandLU and hasPickle:
            UPConfig = uiut.LoadPickle(picklepath)
            attractor = parameters[3].valueAsText 
            aweights =  UPConfig[ts]['aweights'][lu][parameters[3].valueAsText]
            awtvals = []
            if attractor in UPConfig[ts]['aweights'][lu].keys():
                for wt in aweights:
                    awtvals.append([wt[0],wt[1]])
            else:
                awtvals.append([0,0])
            parameters[4].values=awtvals
            

            
            
            
            
        
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        if parameters[0].value:
            if uiut.CheckIfFGDB(parameters[0].value)==False:
                parameters[0].setErrorMessage("You must select a file geodatabase")
        
        
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        UPGDB = parameters[0].valueAsText
        
        picklepath =  "\\".join([UPGDB,"UPConfig.p"])
        if uiut.CheckForPickle(picklepath):
            UPConfig = uiut.LoadPickle(picklepath)
            ts = parameters[1].valueAsText
            lu = parameters[2].valueAsText
            attractor = parameters[3].valueAsText
            
            
            # get attraction weights and submit them to UPConfig.
            values = parameters[4].values
            atweights = [[val[0],val[1]] for val in values]
            
            # sort atweights to be safe
            atweights = sorted(atweights, key=itemgetter(0))
            UPConfig[ts]['aweights'][lu][parameters[3].valueAsText] = atweights
            

            # Write out the pickle
            uiut.MakePickle(UPConfig, UPGDB)
            
            # Write out to tables
            upc.WriteUPConfigToGDB(UPConfig)
            
            messages.addMessage("Attraction Weights Updated")
            print("Attraction Weights Updated.")
        else:
            messages.addMessage("Unable To Update Attraction Weights")
            print("Unable To Update Attraction Weights")
        
        
        return


class SetGeneralPlan(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Set General Plan"
        self.description = "Set the general plan layer to be used in each Time Step"
        self.canRunInBackground = False
        self.category = "C. Scenario Settings"

    def getParameterInfo(self):
        """Define parameter definitions"""
        pUPGDB = arcpy.Parameter(
            displayName="UPlan geodatabase",
            name="UPGDB",
            datatype="DEWorkspace",
            parameterType="Required",
            direction="Input")
        
        pTimeStep = arcpy.Parameter(
            displayName="Select Time Step",
            name="TimeStep",
            datatype="GPString",
            parameterType="Required",
            direction="Input")
        pTimeStep.filter.list = ["Select your UPlan Geodatabase"]
        
        pGeneralPlan = arcpy.Parameter(
            displayName="Select General Plan",
            name="General Plan",
            datatype="GPString",
            parameterType="Required",
            direction="Input")
        pGeneralPlan.filter.list = ["Select your UPlan Geodatabase"]
        
        pGeneralPlanField = arcpy.Parameter(
            displayName="Select General Plan Field",
            name="General Plan Field",
            datatype="Field",
            parameterType="Required",
            direction="Input")
        pGeneralPlanField.parameterDependencies = [pGeneralPlan.name]
        
        params = [pUPGDB,pTimeStep,pGeneralPlan,pGeneralPlanField]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        hasPickle = False
        if parameters[0].value:
            UPGDB = parameters[0].valueAsText
            picklepath = "\\".join([UPGDB,"UPConfig.p"])
            UPConfig = uiut.LoadPickle(picklepath)
            parameters[1].filter.list = [ts[0] for ts in UPConfig['TimeSteps']]
            whereClause = "Role = 'GeneralPlan'"
            cur = arcpy.SearchCursor(os.path.join(UPGDB,'upc_layers'),whereClause,fields="FCName;LongName")
            vals = []
            for row in cur:
                if str(row.getValue("FCName")) not in vals:
                    vals.append(str(row.getValue("FCName")))
            parameters[2].filter.list = vals
            hasPickle = True
        
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        if parameters[0].value:
            if uiut.CheckIfFGDB(parameters[0].value)==False:
                parameters[0].setErrorMessage("You must select a file geodatabase")
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        if parameters[0].value:
            UPGDB = parameters[0].valueAsText
            picklepath = "\\".join([UPGDB,"UPConfig.p"])
            UPConfig = uiut.LoadPickle(picklepath)
            if parameters[1].value and parameters[2].value and parameters[3].value:
                messages.addMessage("Setting General Plan")
                UPConfig[parameters[1].valueAsText]['gp']= [parameters[2].valueAsText, parameters[3].valueAsText]
            
                # Write out the pickle
                uiut.MakePickle(UPConfig, UPGDB)
                
                # Write out to tables
                upc.WriteUPConfigToGDB(UPConfig)
                
                messages.addMessage("General Plan Set")
            else:
                messages.addMessage("Unable to set general plan, check for missing values")
        
        return

class SetGeneralPlanSettings(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Set General Plan"
        self.description = "Set the general plan classes that a lane use can use for a time step"
        self.canRunInBackground = False
        self.category = "C. Scenario Settings"

    def getParameterInfo(self):
        """Define parameter definitions"""
        params = None
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        return

class SetMixedUse(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Set Mixed Use"
        self.description = "Set the mixed use settings for general plan classes by timestep"
        self.canRunInBackground = False
        self.category = "C. Scenario Settings"

    def getParameterInfo(self):
        """Define parameter definitions"""
        params = None
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        return


class PreCalcSubareas(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "1. Precalculate Subareas"
        self.description = "Build subarea-polygon lookup information"
        self.canRunInBackground = False
        self.category = "D. Precalculations"

    def getParameterInfo(self):
        """Define parameter definitions"""
        pUPGDB = arcpy.Parameter(
            displayName="UPlan geodatabase",
            name="UPGDB",
            datatype="DEWorkspace",
            parameterType="Required",
            direction="Input")
        params = [pUPGDB]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        if parameters[0].value:
            if uiut.CheckIfFGDB(parameters[0].value)==False:
                parameters[0].setErrorMessage("You must select a file geodatabase")
                UPGDB = parameters[0].valueAsText
                picklepath = "\\".join([UPGDB,"UPConfig.p"])
                UPConfig = uiut.LoadPickle(picklepath)

        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        UPGDB = parameters[0].valueAsText
#         picklepath = "\\".join([UPGDB,"UPConfig.p"])
#         UPConfig = uiut.LoadPickle(picklepath)
        splitpath = uiut.SplitPath(UPGDB)
        UPConfig = upc.ReadUPConfigFromGDB(splitpath[0],splitpath[1])
        messages.addMessage("Cleaning up Subareas")
        Utilities.UPCleanup_Subareas(UPConfig)
        messages.addMessage("Calculating Subareas")
        CalcSubarea.CalcSA(UPConfig)
        messages.addMessage("Subareas Complete")

        return

class PreCalcGeneralPlans(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "2. Precalculate General Plans"
        self.description = "Build General Plan-polygon lookup information"
        self.canRunInBackground = False
        self.category = "D. Precalculations"

    def getParameterInfo(self):
        """Define parameter definitions"""
        pUPGDB = arcpy.Parameter(
            displayName="UPlan geodatabase",
            name="UPGDB",
            datatype="DEWorkspace",
            parameterType="Required",
            direction="Input")
        params = [pUPGDB]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        if parameters[0].value:
            if uiut.CheckIfFGDB(parameters[0].value)==False:
                parameters[0].setErrorMessage("You must select a file geodatabase")
                UPGDB = parameters[0].valueAsText
                picklepath = "\\".join([UPGDB,"UPConfig.p"])
                UPConfig = uiut.LoadPickle(picklepath)

        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        UPGDB = parameters[0].valueAsText
#         UPGDB = r"..\\testing\calaveras.gdb"
#         picklepath = "\\".join([UPGDB,"UPConfig.p"])
#         UPConfig = uiut.LoadPickle(picklepath)
        splitpath = uiut.SplitPath(UPGDB)
        UPConfig = upc.ReadUPConfigFromGDB(splitpath[0],splitpath[1])
        messages.addMessage("Cleaning up General Plans")
        Utilities.UPCleanup_GeneralPlans(UPConfig)
        messages.addMessage("Calculating General Plans")
        CalcGeneralPlans.CalcGP(UPConfig)
        messages.addMessage("General Plans Complete")

        return

class PreCalcConstraints(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "3. Precalculate Constraints"
        self.description = "Calculate Developable Space based on Constraints"
        self.canRunInBackground = False
        self.category = "D. Precalculations"

    def getParameterInfo(self):
        """Define parameter definitions"""
        pUPGDB = arcpy.Parameter(
            displayName="UPlan geodatabase",
            name="UPGDB",
            datatype="DEWorkspace",
            parameterType="Required",
            direction="Input")
        params = [pUPGDB]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        if parameters[0].value:
            if uiut.CheckIfFGDB(parameters[0].value)==False:
                parameters[0].setErrorMessage("You must select a file geodatabase")
                UPGDB = parameters[0].valueAsText
                picklepath = "\\".join([UPGDB,"UPConfig.p"])
                UPConfig = uiut.LoadPickle(picklepath)

        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        UPGDB = parameters[0].valueAsText
#         UPGDB = r"..\\testing\calaveras.gdb"
#         picklepath = "\\".join([UPGDB,"UPConfig.p"])
#         UPConfig = uiut.LoadPickle(picklepath)
        splitpath = uiut.SplitPath(UPGDB)
        UPConfig = upc.ReadUPConfigFromGDB(splitpath[0],splitpath[1])
        messages.addMessage("Cleaning up Constraints")
        Utilities.UPCleanup_Constraints(UPConfig)
        messages.addMessage("Calculating Constraints")
        CalcConstraints.CalcConstraints(UPConfig)
        messages.addMessage("Constraints Complete")

        return

class PreCalcWeights(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "4. Precalculate Attractor Weights"
        self.description = "Calculate Distances and Weights"
        self.canRunInBackground = False
        self.category = "D. Precalculations"

    def getParameterInfo(self):
        """Define parameter definitions"""
        pUPGDB = arcpy.Parameter(
            displayName="UPlan geodatabase",
            name="UPGDB",
            datatype="DEWorkspace",
            parameterType="Required",
            direction="Input")
        pSaveDisaggWts = arcpy.Parameter(
            displayName="Save Disaggregate Weights (True/False)",
            name="SaveDisaggWts",
            datatype="GPBoolean",
            parameterType="Required",
            direction="Input")
        pSaveDisaggWts.value = False
        params = [pUPGDB,pSaveDisaggWts]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        if parameters[0].value:
            if uiut.CheckIfFGDB(parameters[0].value)==False:
                parameters[0].setErrorMessage("You must select a file geodatabase")
                UPGDB = parameters[0].valueAsText
                picklepath = "\\".join([UPGDB,"UPConfig.p"])
                UPConfig = uiut.LoadPickle(picklepath)

        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        UPGDB = parameters[0].valueAsText
        saveDisaggWts = parameters[0].value
#         UPGDB = r"..\\testing\calaveras.gdb"
#         saveDisaggWts = False
#         picklepath = "\\".join([UPGDB,"UPConfig.p"])
#         UPConfig = uiut.LoadPickle(picklepath)
        splitpath = uiut.SplitPath(UPGDB)
        UPConfig = upc.ReadUPConfigFromGDB(splitpath[0],splitpath[1])
        messages.addMessage("Cleaning up Weights")
        Utilities.UPCleanup_Weights(UPConfig)
        messages.addMessage("Calculating Distances")
        # Get unique set of attractors
        attractors = []
        for TimeStep in UPConfig['TimeSteps']:
            newattractors = UPConfig[TimeStep[0]]['attractors']
            attractors = attractors + newattractors
        attractors = set(attractors)
        CalcWeights.CalcDistanceLayerMultiple(UPConfig,attractors,False) # False disables multi processing.
        
        for TimeStep in UPConfig['TimeSteps']:
            messages.addMessage("Working on time step: {ts}".format(ts = TimeStep[0]))
            CalcWeights.GetWeightsByTs([UPConfig,TimeStep,saveDisaggWts,False]) #[UPConfig,ts,writeResults,runMulti]
            
        messages.addMessage("Weights Complete")

        return
    
class PreCalcAll(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "0. Run All Precalculations"
        self.description = "Calculate Subareas, General Plans, Constraints, Attraction Weights"
        self.canRunInBackground = False
        self.category = "D. Precalculations"

    def getParameterInfo(self):
        """Define parameter definitions"""
        pUPGDB = arcpy.Parameter(
            displayName="UPlan geodatabase",
            name="UPGDB",
            datatype="DEWorkspace",
            parameterType="Required",
            direction="Input")
        pSaveDisaggWts = arcpy.Parameter(
            displayName="Save Disaggregate Weights (True/False)",
            name="SaveDisaggWts",
            datatype="GPBoolean",
            parameterType="Required",
            direction="Input")
        pSaveDisaggWts.value = False
        params = [pUPGDB,pSaveDisaggWts]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        if parameters[0].value:
            if uiut.CheckIfFGDB(parameters[0].value)==False:
                parameters[0].setErrorMessage("You must select a file geodatabase")
#                 UPGDB = parameters[0].valueAsText
#                 picklepath = "\\".join([UPGDB,"UPConfig.p"])
#                 UPConfig = uiut.LoadPickle(picklepath)

        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        UPGDB = parameters[0].valueAsText
        saveDisaggWts = parameters[0].value
#         UPGDB = r"..\\testing\calaveras.gdb"
#         saveDisaggWts = False
#         picklepath = "\\".join([UPGDB,"UPConfig.p"])
#         UPConfig = uiut.LoadPickle(picklepath)
        splitpath = uiut.SplitPath(UPGDB)
        UPConfig = upc.ReadUPConfigFromGDB(splitpath[0],splitpath[1])
        
        
        
        # Subareas
        messages.addMessage("Cleaning up Subareas")
        Utilities.UPCleanup_Subareas(UPConfig)
        messages.addMessage("Calculating Subareas")
        CalcSubarea.CalcSA(UPConfig)
        messages.addMessage("Subareas Complete")
                
        
        # General Plans
        messages.addMessage("Cleaning up General Plans")
        Utilities.UPCleanup_GeneralPlans(UPConfig)
        messages.addMessage("Calculating General Plans")
        CalcGeneralPlans.CalcGP(UPConfig)
        messages.addMessage("General Plans Complete")
        
        
        # Constraints
        messages.addMessage("Cleaning up Constraints")
        Utilities.UPCleanup_Constraints(UPConfig)
        messages.addMessage("Calculating Constraints")
        CalcConstraints.CalcConstraints(UPConfig)
        messages.addMessage("Constraints Complete")
        
        
        # Attraction Weights
        messages.addMessage("Cleaning up Weights")
        Utilities.UPCleanup_Weights(UPConfig)
        messages.addMessage("Calculating Distances")
        # Get unique set of attractors
        attractors = []
        for TimeStep in UPConfig['TimeSteps']:
            newattractors = UPConfig[TimeStep[0]]['attractors']
            attractors = attractors + newattractors
        attractors = set(attractors)
        CalcWeights.CalcDistanceLayerMultiple(UPConfig,attractors,False) # False disables multi processing.
        
        for TimeStep in UPConfig['TimeSteps']:
            messages.addMessage("Working on time step: {ts}".format(ts = TimeStep[0]))
            CalcWeights.GetWeightsByTs([UPConfig,TimeStep,saveDisaggWts,False]) #[UPConfig,ts,writeResults,runMulti]
            
        messages.addMessage("Weights Complete")

        return

class RunAllocation(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Run Allocation"
        self.description = "Run the full allocation of UPlan."
        self.canRunInBackground = False
        self.category = "E. UPlan Run"

    def getParameterInfo(self):
        """Define parameter definitions"""
        pUPGDB = arcpy.Parameter(
            displayName="UPlan geodatabase",
            name="UPGDB",
            datatype="DEWorkspace",
            parameterType="Required",
            direction="Input")
        params = [pUPGDB]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        if parameters[0].value:
            if uiut.CheckIfFGDB(parameters[0].value)==False:
                parameters[0].setErrorMessage("You must select a file geodatabase")
                UPGDB = parameters[0].valueAsText
                picklepath = "\\".join([UPGDB,"UPConfig.p"])
                UPConfig = uiut.LoadPickle(picklepath)

        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        UPGDB = parameters[0].valueAsText
#         UPGDB = r"..\testing\calaveras.gdb"
        #picklepath = "\\".join([UPGDB,"UPConfig.p"])
        #UPConfig = uiut.LoadPickle(picklepath)
        
        splitpath = uiut.SplitPath(UPGDB)
        UPConfig = upc.ReadUPConfigFromGDB(splitpath[0],splitpath[1])
        messages.addMessage("Cleaning up Allocation")
        Utilities.UPCleanup_Alloc(UPConfig)
        messages.addMessage("Running Allocation")
        Allocation.Allocate(UPConfig)

        return

class ZonalSummary(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "1. Zonal Summary"
        self.description = "Summarize UPlan outputs by zone"
        self.canRunInBackground = False
        self.category = "F. Analysis"

    def getParameterInfo(self):
        """Define parameter definitions"""
        params = None
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        return

#For Debugging
# def main():
#     tbx = Toolbox()
#     tool = RunAllocation()
#     tool.execute(tool.getParameterInfo(), None)
#                
# if __name__ == '__main__':
#     main()
      
    

    