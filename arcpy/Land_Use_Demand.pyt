import arcpy


class Toolbox(object):
    def __init__(self):
        """Define the toolbox (the name of the toolbox is the name of the
        .pyt file)."""
        self.label = "Land Use Demand"
        self.alias = "lu_demand"

        # List of tool classes associated with this toolbox
        self.tools = [Demographics]


class Demographics(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Input Demographics"
        self.description = ""
        self.canRunInBackground = False

    def getParameterInfo(self):
        """Define parameter definitions"""
        #First Parameter - Base Population
        BasePop = arcpy.Parameter(
            displayName="Base Population",
            name="base_pop",
            datatype="GPLong",
            parameterType="Required",
            direction="Input")
        
        #Second Parameter - Future Population
        FuturePop = arcpy.Parameter(
            displayName="Future Population",
            name="future_pop",
            datatype="GPLong",
            parameterType="Required",
            direction="Input")
        
        #Third Parameter - PPHH
        PPHH = arcpy.Parameter(
            displayName="Persons Per Household",
            name="pphh",
            datatype="GPDouble",
            parameterType="Required",
            direction="Input")
        
        #Fourth Parameter - EPHH
        EPHH = arcpy.Parameter(
            displayName="Employees Per Household",
            name="ephh",
            datatype="GPDouble",
            parameterType="Required",
            direction="Input")
        
        params = [BasePop,FuturePop,PPHH,EPHH]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        return
