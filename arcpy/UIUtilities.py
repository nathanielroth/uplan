'''
Created on Jun 9, 2015

@author: roth
'''

import arcpy
import os
import cPickle as pickle

def CheckForPickle(path):
    return os.path.isfile(path)


def SplitPath(path):
    dbpath = path[:path.rfind("\\")]
    dbname = path[(path.rfind("\\")+1):]
    return([dbpath,dbname])



def LoadPickle(path):
    
    #updates the path information in UPConfig incase it has moved. 
    fulldb = path[:path.rfind("\\")]
    dbpath = fulldb[:fulldb.rfind("\\")]
    dbname = fulldb[(fulldb.rfind("\\")+1):]
    UPConfig = pickle.load( open(path, "rb"))
    UPConfig['paths']['dbpath'] = dbpath
    UPConfig['paths']['dbname'] = dbname
    return(UPConfig)
    
def MakePickle(UPConfig, path):
    picklepath = os.path.join(path,"UPConfig.p")
    pickle.dump(UPConfig, open(picklepath, "wb"))
    return

def InsertToList(lst,item,position):
    return(lst[:(position-1)] + [item] + lst[(position-1):])
    
def MakeEmptyTimeStep():
    ts = {}
    ts['gp'] = []
    ts['Demand'] = {}
    ts['gplu'] = {}
    ts['mixeduse'] = {}
    ts['constraints'] = []
    ts['cweights'] = {}
    ts['attractors'] = []
    ts['aweights'] = {}
    
 
    return(ts)

def CheckIfFGDB(path):
    desc = arcpy.Describe(path)
    if desc.workspaceFactoryProgID == "esriDataSourcesGDB.FileGDBWorkspaceFactory.1":
        return(True)
    else:
        return(False)

def CheckIfTSInFGDB(path,ts):
    whereClause = "Code = '{ts}'".format(ts = ts)
    arcpy.MakeTableView_management(os.path.join(path,"upc_timesteps"),"tblview",whereClause)
    count = arcpy.GetCount_management("tblview")
    arcpy.Delete_management("tblview")
    if count > 0:
        return(False)
    else:
        return(True)



# testing
if __name__ == "__main__":
    dbpath = r"..\testing" 
    dbname = 'calaveras.gdb' 
    fullpath = os.path.join(dbpath,dbname)
    
    fullpath = r'C:\Projects\UPlan4\testing\calaveras.gdb'
    sppath = SplitPath(fullpath)
    print(sppath)
    