This repository will be the holding point for all content related to the redevelopment of UPlan under contract with the Amador County Transportation Commission.

The project officially started in December of 2014, but will really be kicking off with the start of 2015.

For any information, please contact Nathaniel (Nate) Roth at neroth@ucdavis.edu