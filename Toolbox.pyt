#author: bjorkman, boynton

import arcpy
import LayerMangement



class Toolbox(object):
    def __init__(self):
        self.label = "ImportTool"
        self.alias = "import"

        # List of tool classes associated with this toolbox
        self.tools = [ImportBaseGeom] # a list of the tools to have available.


class ImportBaseGeom(object):
    def __init__(self):
        #Import a feature class layer to the existing geodatabase as a base layer
        self.label = "Import Base Geometry Layer"
        self.description = ""
        self.canRunInBackground = True

    def getParameterInfo(self):
        """Define parameter definitions"""
        
         # First parameter
         in_features = arcpy.Parameter(
            displayName="Input Feature Class",
            name="in_features",
            datatype="GPFeatureLayer",
            parameterType="Required",
            direction="Input")
	
	    # Second parameter
         out_features = arcpy.Parameter(
	        displayName="UPlan File Geodatabase",
	        name="out_features",
	        datatype="GPFeatureLayer",
	        parameterType="Derived",
	        direction="Output")

        #out_features.parameterDependencies = [in_features.name]
        #out_features.schema.clone = True

        params = [in_features, out_features]
        return parameters

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        #Get inputs        
        inFeatures = parameters[0]
        outFeatures = parameters[1]
        
        LayerManagement.ImportBaseGeom(inFeatures, outFeatures)
        
		
		return