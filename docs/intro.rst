Introduction to UPlan
=====================

Goals
-----

UPlan is a simple urban growth model. The goals are to allow for the rapid development and implementation of a GIS based model projecting urban growth for a small region. Inputs should be able to be locally derived, and be available to most regions with modest data preparation.

This goal precludes the some forms of complex tools and limits our ability to handle complex economic interactions. If addressing complex, interregional interactions are a priority, UPlan is not the appropriate tool, and I encourage you to look at PECAS, UrbanSim 2, MUSSA II, or other models more able to handle those factors.

History of UPlan
----------------

Version 1
+++++++++

Circa 2000-2003

Bob Johnston, David Shabazian, Shengyi Gao, Eric Lehmer, Chad Shook 

Written in: ArcView Avenue

This version was used for several early applications of UPlan including work in and around Los Alamos following the fire and planning for reconstruction, the Merced Model Improvement Program, and the Deleware Valley. 


Version 2.X
+++++++++++

2003-2015

Bob Johnston, David Shabazian, Shengyi Gao, Eric Lehmer, Nathaniel Roth

Written in: VBA (ArcObjects)

Version 2.X received much wider use across more than twenty counties in the state of California as part of the San Joaquin Valley Partnership, San Joaquin Valley Blueprint, the Regional Blueprint projects (non-MPO RTPAs), several smaller specific projects within the state, across the US, and internationally. Additionally this version was extended to develop direct connections to several travel demand models (TransCAD, and EMME) and remains in use by several county level organizations in California.

Version 3
+++++++++

2012

Written in: VBA (ArcObjects)

This version had very little use. It remains fully operational, but was written just as ESRI announced the discontinuation of support for VBA and so little emphasis was placed on it's use.

Version 4
+++++++++

2015

Written in: Python (Arcpy)

Version 4 is currently being written as an update to UPlan to preserve it's functionality into a next generation of code for ArcGIS.
