Meeting Notes
=============

February 26, 2015
-----------------

Agenda: 
_______


#. Welcome & Introductions
#. Proposed programming languages (Nate)
#. Proposed features (Nate)
#. Timelines (Nate)
#. Billing and Administration

Notes:
______

Nate provided an overview of the current status and plans for the continued development.

We discussed:
Software platform: ESRI and Arcpy based
Results of the informal online survey conducted by Nate of the past and present UPlan users.
The proposed route forward and had general agreement on the plan as described in the development document.


January 14, 2015
----------------

Initial meeting for UPlan 4 development

Agenda
______

1. UPlan status update for each county (county/RTPA reps)
2. Steps and time frames for UPlan update (Nate)
3. Financial Needs/Management
4. County Responsibilities/Tasks (Group)

  A. Short Term
  B. Long Term

5. UPlan4

  A. Git    
  B. Technologies    
  C. Methods
	

Notes
_____

UPlan Status for counties:
++++++++++++++++++++++++++

**Tuolumne:**
 
UPlan began with training in 2004.
It's been a long time with lots of gaps. Actually using it in 2005(ish). The county created an Oversight Committee and a Technical Advisory Committee (the two of which ended up dissolving). Had issues with presenting too much detail to the committees resulting in burnout. It took a couple years to agree on model inputs and was completed in ~2010-11. By that point the public interest was low due to the recession and some of the inputs also needed to be updated. Ending with a fizzle. 
Since then, to comply with GHG regulations, some additional analysis has been done which suggested some changes to the GP. Given the political shifts, developers have significant leeway to move as they wish. 

Have a process going on where the General Plan is being made consistent with zoning (rather than the other way around). 

UPlan past: Adopted the Distinctive Communities plan in ~2010-11 as the conclusion of the Blueprint.

**Calaveras:**


Completed the Blueprint process, with the new planning director integrated UPlan into the GP EIR process. Draft GP Released in December has references to UPlan. 

**Amador:**

See ACTC report (approved by board on Dec. 19, 2014), with the work and the findings. Amador County continues to have challenges with the politics of the planning environment regarding the adoption of UPlan as a countywide planning tool. 
Latest version: 
ACTC was told to use the draft county GP, and the most recent city general plans, and instructed to stick to transportation planning. (separation of land use and transportation planning). ACTC has been finding that the UPlan results are very useful for ongoing transportation planning. 

Subsequently, the county came back to ask for UPlan outputs for their own business processes (circulation element, housing element). 

Cindy will be doing a lot of data sorting and cleanup. 

ACTC Board wants to see and approve any outward facing versions of the data. 

ACTC UPlan run versions are carefully tracked with a structured naming convention. X.0 is the release version, X.1+ are in-house working products. 

Contracting:
++++++++++++
Cal-Tuolumne: split ~$53k
Amador: ~$70k
UCD: $95k (phase1), $95k(Phase2). 

Billing: 
UCD: Monthly (with supporting documentation)

Robin will need to invoice for the local match from the counties to cover the 14/15 FY outlay. 

Time Lines:
+++++++++++

(enhanced from our discussion)
By Mid-February: Nate will deliver examples of the methods to be used in UPlan
Second week of February: Counties will provide feedback on the methods
By End of February: Nate will deliver a development plan with details about the methods to be used. (Deliverable for Task 3.1.1)
By End of June: Beta version of UPlan will be available for testing by counties.
By End of June: Draft (possibly coarse) documentation of UPlan's methods and use will be available to the counties.
August 31, End of Contract for Phase 1.


Other Notes:
++++++++++++

Road Centerlines may be available from Vestra for the full state. Being done for CalFire Dispatch. 

UPlan Functional Needs:
+++++++++++++++++++++++

Better ability to simulate:

* Rural employment location (winery)
* Farmworker Housing

Primary Goals:

* Mixed use
* Sequential time steps 
* Subarea support

Run Tracking/Metadata

Post Run Analysis:

* TAZ Export
* Zonal Summary

Requests:
Thiessen Polygon Creation for building primary datasets. 
Pseudoparcel creation (automate breaking down larger parcels into smaller pieces to avoid having to apply land uses to larger areas than necessary). This tool requires ArcGIS Desktop Advanced licensing. 


UPlan Technologies:
+++++++++++++++++++

Nate suggested three possible top level architectures:

1. ESRI with Python/ArcPY (model) and C# (User Interface)
2. QGIS using Python and PostGIS
3. Django using Python and PostGIS (basically a web application that can be run on a desktop)

The group appeared to be fairly committed to remaining on the ESRI Platform using ArcPy.
Nate has a prototype written using arcpy that has very basic functionality (no user interface) that runs in arcpy in 17 min.

Nate is working on an open source based version (Update, I've got it working but it's more complex and would require more skilled other folks to update). 

If the "model" code is written using Arcpy, I will need to develop the UI in C# as an ArcGIS Add-in or extension.

However, keeping this in ESRI will require that users have either an ArcGIS Desktop Advanced License, or a Spatial Analyst Extension License, potentially resulting in additional licensing costs. 

The two options below have the advantage of having no licensing costs and possibly allowing for wider use in non-esri environments or where budget for licensing is a problem.

The QGIS option will require that users develop comfort with QGIS and to a lesser extent with PostGIS. 

Django will be a complex installation and will pose some learning challenges to the staff and might have issues with IT department acceptance. However, it could be exposed to the internet for outready purposes.
