.. _progressreports:
Progress Reports
================

First Quarter 2015
------------------
**April 13, 2015**

During the first quarter of 2015, we (ICE, UC Davis) accomplished the following:

Accomplishments:
++++++++++++++++

* An openly viewable code repository has been established at: https://bitbucket.org/nathanielroth/uplan

  * All code in it's current state is visible, and downloadable
  * All documentation source material is stored in its most current state
  * Documentation (including this document) are available at: http://uplan.readthedocs.org/ (The source documentation on bitbucket.org is automatically updated onto readthedocs.org each time it is updated)
  
* The initial system design was completed and is documented at: :ref:`Link <devplans>`
* Some of the key concepts from the system design document are:

  * A move to a polygon based structure with the use of a "base geometry" dataset that can be derived from multiple sources (likely but not necessarily, polygons)
  * Constraints replace masks and allow for tailored reductions in developablity specific to each land use. 
  * Built in vertical mixed use, improved redevelopment, and time step functionality
  * Written primarily in Python (for Arcpy), with possible user interface components in C# as an ArcGIS add-in.
  
* As part of the system design an informal survey was conducted with the following notable results

  * The user group expressed a strong preference for staying in the Esri environment
  * Maintaining the ability to run UPlan with simple demographics has a high priority
  * Deterministic allocation is important to most users
  * TAZ export functionality (preferably with the custom export) is essential
  * Vertical mixed use, redevelopment, and time steps support are strongly desired
  * Other desired, but not as critical components are the use of past growth to develop a statistical calibration of attracters and weighted random allocation, . 
  
* Much of the UPlan system was prototyped within arcpy
  
  * Results in arcpy were very positive for being able to complete the work
  * The performance of the prototype in arcpy with run times in the ~5-7 minute range.

* Translation of prototype into deployable code (Python with Arcpy) began and is proceeding well

  * A prototype dataset has been constructed for use (based on Calaveras County Data)
  * A simplified, but not oversimplified set of settings (general plan, attractor, constraint, and land use demand) are in use.
  * Additional (free with installation instructions coming) Python libraries will be needed to run UPlan. This includes Pandas, and a prerequisite update of the numpy library installed with ArcGIS's installation of Python.

* Individual components of the full model are running, with reasonable run times, and expected results for:

  * General Plans: Specifying which land uses are permitted in each area including mixed-use.
  * Attraction Weights: Calculating weights for each base geometry polygon for each land use.
  * Constraints: Calculating the developable space in each base geometry polygon for each land use as reduced by the overlapping constraints.
  
* The following components are in active development or in testing

  * UPlan configuration settings: read/write to and from a file geodatabase
  * Allocation: Assigning land uses to polygons based on general plans, constraints, attractions, and allowed mixed use combinations.

Current Programming Status:
+++++++++++++++++++++++++++

**Base Geometries:** a "Base Geometry" dataset is the collection of all parcels or other polygons that will be used to track land use change. A full base geometry set includes two layers: the polygon boundaries, and the polygon centroids linked by a common identifier unique to each polygon. Primary use of the base geometry set is operational and is being used by almost all subsequent components of UPlan.

**UPlan Configuration:** The UPlan configuration settings and storage methods are developed and in use, but continue to evolve as new features are added. 

**General Plan:** Land availability for land uses is implemented and operational. The developablity of each polygon is determined by the General Plan category that it's centroid falls into. Mixed use settings are currently undergoing testing.

**Constraints:** Developable space for each polygon is reduced for each land use based on the strength of the constraint and the portion of the polygon overlapped by that constraint. Where multiple constraints overlap, their effects are cumulative. This is coded and operational.

**Attractions:** The calculation of attraction weights based on the distance from the attraction layer to the centroid of each polygon and then weighted by the user specified factors is coded and operational.

**Subareas:** The calculations for which polygon falls into each subarea are operational and in use. A polygon is assigned to the subarea that its centroid falls into.

**Time Steps:** The time step settings exist, and are coded, but largely untested while the allocation components are being completed.

**Allocation:** (Broken into subcategories)
 
  * **Allocation:** The initial allocation of land uses by time step and subarea to available space is operational, but still under active development.
  * **Allocation Evaluation:** This process checks the primary allocation for conflicts, and removes land uses that conflict with higher priority land uses. These are then submitted back to another iteration of the allocation process for reallocation. 
  * **Mixed Use:** This evaluation step is the point at which mixed use are evaluated to be sure that they are permitted under the general plan settings. Coded and in early testing.
  * **Redevelopment:** Not yet implemented. The connection points for inserting redevelopment functions exist and await implementation.

Next Steps:
+++++++++++

* Complete the testing of Allocation functions including redevelopment, mixed use, and time steps. 
* Set the initial Land Use Demand methods (See methods 1, 2, and 3 from the Development Page)
* Connect the UPlan tools to a Python Toolbox for easier use within ArcGIS
* Develop a graphical user interface for managing General Plan, Attractor, Constraints, and model configuration settings.
* Develop data preparation and loading tools
* Develop post-run analytical tools for exporting results to zones or TAZs.
* Continue to improve documentation.