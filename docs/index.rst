.. UPlan4 documentation master file, created by
   sphinx-quickstart on Mon Jan  5 10:32:22 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to UPlan4's Documentation!
==================================

.. toctree::
   :maxdepth: 2

   intro
   development
   usermanual
   meetingnotes
   progressreports
   codedocs
